package card;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


public class BoardTest {

	public Board board;

	public static List<Card> cards = new ArrayList<Card>();

	@Before
	public void setUp() throws Exception {
		Board.initialiseLocations();
	}

	@Test
	public void testGetCards() {

		assertEquals(Board.getCards().get(0).getName(), "Garden Centre");

	}

	@Test
	public void testGetACard1()
	{	
		assertEquals(Board.getACard(0).getName(),"Garden Centre");	;

	}
	
	@Test
	public void testGetACard2()
	{	
		assertEquals(Board.getACard(20).getName(),"Public Park");	

	}
	
	/**
	 * Testing an out of bounds card
	 * !!! IMPORTANT !!!
	 * Works when only board test is ran but doesn't work when all of the 
	 * junit tests are ran at once.
	 */
	
	@Test
	@Ignore
	public void testGetACardOutOfBound()
	{
		try{
			Board.getACard(100);
			fail("Should have thrown exception");
		   }
		   catch(Exception e){
		      
		   }
	}
}
