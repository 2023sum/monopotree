package card;


import card.Board;
import utilities.ImportLuckyDip;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;



public class LuckyDipPositionCardTest {
	

	public static String luckyDipCards[];
	public Board board;
	public LuckyDipPositionCard card;
	
	
	/**
	 * Constructing the object to use in tests
	 */
	@Before
	public void setUp() throws Exception
	{
		String name = "TestName";
		boolean canBuy = false;
		boolean obtainable = false;
		this.card = new LuckyDipPositionCard(name,canBuy,obtainable);
		Board.initialiseLocations();
		
	}
	
	@Test
	public void testGettters()
	{
		assertEquals(card.getName(),"TestName");
		assertEquals(card.canBuy(),false);
		assertEquals(card.getObtainable(),false);
		
	}
	
	@Test
	public void testSetCanBuy()
	{
		card.setCanBuy(true);
		assertEquals(card.canBuy(),true);
	}
	
	@Test
	public void testSetObtainable()
	{
		card.setObtainable(true);
		assertEquals(card.getObtainable(),true);
	}
	
	@Test
	public void testGetCard()
	{
		assertEquals(LuckyDipPositionCard.getCard(0),"Advance to \"GARDEN CENTRE\". Collect 200 seeds.");
	}
	
	@Test
	public void testGetCardOutsideRange()
	{
		assertEquals(LuckyDipPositionCard.getCard(16),"Index is out of bounds for the list of cards! ");
	}

}
