package card;

import card.Board;
import utilities.ImportLuckyDip;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TreasureChestPositionCardTest {

	public static String TreasureChestCards[];
	public Board board;
	public TreasureChestPositionCard card;

	/**
	 * Constructing the object to use in tests
	 */
	@Before
	public void setUp() throws Exception {
		String name = "TestName";
		boolean canBuy = false;
		boolean obtainable = false;
		this.card = new TreasureChestPositionCard(name, canBuy, obtainable);
		Board.initialiseLocations();

	}

	@Test
	public void testGettters() {
		assertEquals(card.getName(), "TestName");
		assertEquals(card.canBuy(), false);
		assertEquals(card.getObtainable(), false);

	}

	@Test
	public void testSetCanBuy() {
		card.setCanBuy(true);
		assertEquals(card.canBuy(), true);
	}

	@Test
	public void testSetObtainable() {
		card.setObtainable(true);
		assertEquals(card.getObtainable(), true);
	}

	@Test
	public void testGetCard() {
		assertEquals(TreasureChestPositionCard.getCard(0), "Advance to \"GARDEN CENTRE\". Collect 200 seeds.");
	}

	@Test
	public void testGetCardOutsideRange() {
		assertEquals(TreasureChestPositionCard.getCard(16), "Index is out of bounds for the list of cards! ");
	}

}
