package locations;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import card.Board;
import card.Card;
import locations.Transport;
import person.Player;



/**
 * Testing the transport class and the properties abstract class 
 * which it extends
 */

public class TransportPropertiesTest {
	
	Card transport; 		// The transport declared for testing
	Card transport1;		// Second transport card declared for testing
	Player owner;			// Player object who will own the transport
	Player player;			// Player who will pay the donation
	Transport boatClub;	// Boat club for testing
	Transport cyclePath; // Cycle path for testing (One of the transports)
	ArrayList<Card> cards;  // List of all cards on the board
	
	/**
	 * Initialising all of the cards and getting a transport card out of the board
	 * @throws Exception
	 */
	
	@Before
	public void setUp() throws Exception {
		Board.initialiseLocations();
		transport = Board.getACard(5);
		cyclePath = (Transport) transport;
		transport1 = Board.getACard(15);
		boatClub = (Transport) transport1;
		player = new Player("Michal");
		owner = new Player("James");
		cards = (ArrayList<Card>) Board.getCards();
	}
	
	/**
	 * Testing to see if taking the donation away works
	 * 
	 * !!! IMPORTANT !!!
	 * When this test class is ran by itself then the test passes.
	 * But if it's ran all together it fails
    */

	@Test
	@Ignore
	public void testTakeDonation() {
		owner.buyLocation(5);
		cyclePath.setOwnedBy(owner);
		cyclePath.takeDonation(player);
		assertEquals(player.getBalance(),1475);
		assertEquals(owner.getBalance(),1525);
	}
	
	/**
	 * Taking the correct amount of donation after landing on a lucky dip
	 */

	@Test
	public void testTakeRentLuckyDip() {
		owner.buyLocation(15);
		boatClub.setOwnedBy(owner);
		boatClub.takeDonationLuckyDip(player);
		assertEquals(player.getBalance(),1450);
		assertEquals(owner.getBalance(),1550);
	}

	/**
	 * Testing if getting the correct donation value works
	 */

	@Test
	public void testGetCurrentDonation() {
		assertEquals(cyclePath.getCurrentDonation(),100);
		
	}
	
	/**
	 * Testing taking the location away from someone
	 */
	
	@Test
	public void testTakeLocation() {
		cyclePath.takeLocation(owner);
		assertEquals(cyclePath.getOwnedBy(),null);
	}
	
	/**
	 * Testing if the price is correct for the certain location
	 */

	@Test
	public void testGetPrice() {
		assertEquals(cyclePath.getPrice(),200);
	}

}
