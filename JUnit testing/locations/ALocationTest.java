package locations;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import card.Board;
import card.Card;
import locations.ALocation;
import person.Player;

/**
 * Testing the Aroperty class
 *
 */

public class ALocationTest {
	
	Card location; 	     	// The card declared for testing
	Player owner;			// Player object who will own the train station
	Player player;			// Player who will pay the rent
	ALocation borneo; 		// Location that will be tested
	ArrayList<Card> cards;  // List of all cards on the board
	
	/**
	 * Declaring all of the needed objects and importing the board
	 */
	
	@Before
	public void setUp() throws Exception {
		Board.initialiseLocations();
		cards = (ArrayList<Card>) Board.getCards();
		location = cards.get(1);
		borneo = (ALocation) location;
		player = new Player("James");
		owner = new Player("Michal");
	}

	/**
	 * Testing taking rent for the location 
	 */

	@Test
	public void testTakeRent() {
		owner.buyLocation(1);
		borneo.setOwnedBy(owner);
		borneo.takeDonation(player);
		assertEquals(player.getBalance(),1470);
		assertEquals(owner.getBalance(),1530);
	}
	
	/**
	 * Testing if it successfully adds a tree to the location
	 */

	@Test
	public void testAddTree() {
		borneo.addTree();
		assertEquals(borneo.getNoOfTrees(),1);
	}
	
	@Test
	public void testAddTreeLimit()
	{
		borneo.addTree();
		borneo.addTree();
		borneo.addTree();
		borneo.addTree();
		assertEquals(borneo.addTree(), "You can't add a tree here anymore");
	}
	
	/**
	 * Test if the getter for if the location is owned works
	 */

	@Test
	public void testGetOwned() {
		assertEquals(borneo.getOwned(),false);
	}
	
	/**
	 * Test getting the current amount of rent
	 */

	@Test
	public void testGetCurrentLocation() {
		// Test with a forest
		assertEquals(borneo.getCurrentDonation(),250);
	
	}
	
	/**
	 * Testing getting the price of a tree
	 */

	@Test
	public void testGetTreePrice() {
		assertEquals(borneo.getTreePrice(),50);
	}

	/**
	 * Testing getting how many trees there are on this location
	 */
	
	@Test
	public void testGetNoOfTrees() {
		borneo.addTree();
		assertEquals(borneo.getNoOfTrees(),2);
	}

  }
