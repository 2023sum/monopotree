package locations;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import card.Board;
import card.Card;
import locations.Utility;
import person.Player;

/**
 * Testing the utility class
 */

public class UtilityTest {
	
	Card utility;			// The card declared for testing
	Player owner;			// Player object who will own the location
	Player player; 			// Player who will pay the donation
	Utility solarPower;		// Utility that will be tested
	Card utility1;			// Second card declared for testing
	Utility hydroPower;		// Second utility that will be tested
	ArrayList<Card> cards;  // Lost of all the cards on the board
	
	/**
	 * Declaring all of the needed objects for testing the utility class
	 */

	@Before
	public void setUp() throws Exception {
		Board.initialiseLocations();
		cards = (ArrayList<Card>) Board.getCards();
		utility = cards.get(12);
		utility1 = cards.get(28);
		hydroPower = (Utility) utility1;
		solarPower = (Utility) utility;
		player = new Player("James");
		owner = new Player("Michal");
	}

    /**
     * Testing taking the donation from the player 
     * Using a roll of 4
     * 
     * !!! IMPORTANT !!!
	 * When this test class is ran by itself then the test passes.
	 * But if it's ran all together it fails
     */

	@Test
	@Ignore
	public void testTakeDonation() {
		owner.buyLocation(12);
		solarPower.setOwnedBy(owner);
		solarPower.takeDonation(4, player);
		assertEquals(player.getBalance(),1468);
		assertEquals(owner.getBalance(),1532);
	}

	/**
	 * Testing taking donation after landing on the utility due to a lucky dip
	 */
	
	@Test
	public void testTakeDonationLuckyDip() {
		owner.buyLocation(28);
		hydroPower.setOwnedBy(owner);
		hydroPower.takeDonationLuckyDip(4, player);
		assertEquals(player.getBalance(),1420);
		assertEquals(owner.getBalance(),1580);
	}
	
	/**
	 * Testing getting the cost of the utility
	 */

	@Test
	public void testGetCost() {
		assertEquals(solarPower.getCost(),150);
	}

}
