package backFromTheBrink;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import backFromTheBrink.CardControl;
import card.Board;
import locations.ALocation;
import person.Player;
import utilities.PlayerStorage;

/**
 * Test class for the card control class
 */

public class CardControlTest {
	
	int index; // Which treasure chest card will be called
	PlayerStorage playerStorage; // The player storage class
	Player player; // The player object
	int rollNumber; // The amount rolled
	int playerIndex; // The index of which player this is in the playerstorage player list
	static Scanner input; // Scanner for the lucky dip class
	
	/**
	 * Setting up the scanner so it only declares it once
	 */
	
	@BeforeClass
	public static void setUpBefore() throws Exception {
		input = new Scanner(System.in);
	}
	
	/**
	 * Setting up the needed variables/objects for this test
	 * @throws Exception
	 */

	@Before
	public void setUp() throws Exception {
		playerStorage = new PlayerStorage();
		playerStorage.addPlayer("Michal");
		playerStorage.addPlayer("James");
		player = playerStorage.getPlayerList().get(0);
		rollNumber = 6;
		playerIndex = 0;
		Board.initialiseLocations();
	}

	/**
	 * Testing the zero case of the lucky dip
	 */
	
	@Test
	public void testUseTreasureChestCase0() {
		// Setting up the index
		index = 0;
		
		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
			    
	    // Calling the output
	    CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

	    // Testing if the output is correct
	    String expectedOutput  = "200 seeds were added to Michal's balance!"; 
			
	    assertEquals(expectedOutput,outContent.toString().trim());
	    assertEquals(player.getPosition(),0);
	}
	
	/**
	 * Testing the first case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase1() {
		// Setting up the index
		index = 1;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Michal has been sent to the poacher's trap!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),10);
	}
	
	/**
	 * Testing the second case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase2() {
		// Setting up the index
		index = 2;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "100 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1600);
	}
	
	/**
	 * Testing the third case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase3() {
		// Setting up the index
		index = 3;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "50 seeds were taken from Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1450);
	}
	
	/**
	 * Testing the fourth case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase4() {
		// Setting up the index
		index = 4;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "100 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1600);
	}
	
	/**
	 * Testing the fifth case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase5() {
		// Setting up the index
		index = 5;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "50 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1550);
	}
	
	/**
	 * Testing the sixth case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase6() {
		// Setting up the index
		index = 6;
		
		// Calling the method
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);
		
		assertEquals(playerStorage.getPlayerList().get(1).getBalance(),1490);
		assertEquals(playerStorage.getPlayerList().get(0).getBalance(),1510);
	}
	
	/**
	 * Testing the seventh case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase7() {
		// Setting up the index
		index = 7;
		
		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "20 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1520);
	}
	
	/**
	 * Testing the eighth case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase8() {
		// Setting up the index
		index = 8;
		
		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "100 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1600);
	}
	
	/**
	 * Testing the ninth case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase9() {
		// Setting up the index
		index = 9;
		
		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "80 seeds were taken from Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1420);
	}
	
	/**
	 * Testing the tenth case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase10() {
		// Setting up the index
		index = 10;
		
		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "20 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1520);
	}
	
	
	/**
	 * Testing the eleventh case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase11() {
		// Setting up the index
		index = 11;
		
		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "40 seeds were taken from Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1460);
	}
	
	/**
	 * Testing the twelfth case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase12() {
		// Setting up the index
		index = 12;
		
		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "50 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1550);
	}
	
	/**
	 * Testing the thirteenth case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase13() {
		// Setting up the index
		index = 13;
		
		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "25 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1525);
	}
	
	/**
	 * Testing the fourteenth case of the the treasure chest
	 */
	
	@Test
	public void testUseTreasureChestCase14() {
		// Setting up the index
		index = 14;
		
		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "100 seeds were taken from Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1400);
	}
	
	/**
	 * Testing the fifteenth case of the the treasure chest
	 * 
	 *  !!!!!! IMPORTANT !!!!!!!!!!!
	 * Works when this test is ran by itself without interference of all 
	 * of the other tests
	 */
	
	@Test
	@Ignore
	public void testUseTreasureChestCase15() {
		// Setting up the index
		index = 15;
		
		// Adding a tree so it takes away 10 seeds
		ALocation borneo = (ALocation) Board.getACard(1);
		borneo.addTree();
		player.buyLocation(1);
		player.buyTree();
		
		// Calling the output
		CardControl.useTreasureChest(index, playerStorage, player, rollNumber, playerIndex);

		assertEquals(player.getBalance(),1490);
	}
	
	/**
	 * Testing the zero case of the lucky dip
	 */
	
	@Test
	public void testUseLuckyDipCase0() {
		// Setting up the index
		index = 0;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useLuckyDip(index, playerStorage, player, input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "200 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1700);
	}
	
	/**
	 * Testing the first case of the lucky dip with position less than 29
	 */
	
	@Test
	public void testUseLuckyDipCase1PositionLessThan29() {
		// Setting up the index
		index = 1;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useLuckyDip(index, playerStorage, player, input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to GANSU!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),29);
	}
	
	/**
	 * Testing the first case of the lucky dip with position more than 29
	 */
	
	@Test
	public void testUseLuckyDipCase1PositionMoreThan29() {
		// Setting up the index
		index = 1;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Setting position to 30
		player.setPosition(30);
		
		// Calling the output
		CardControl.useLuckyDip(index, playerStorage, player, input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "200 seeds were added to Michal's balance!\r\n"
				+ "Moved to GANSU!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1700);
		assertEquals(player.getPosition(),29);
	}
	
	/**
	 * Testing the second case of the lucky dip with position less than 24
	 */
	
	@Test
	public void testUseLuckyDipCase2PositionLessThan24() {
		// Setting up the index
		index = 2;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useLuckyDip(index, playerStorage, player, input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to PARAGUAY!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),24);
	}
	
	/**
	 * Testing the second case of the lucky dip with position more than 24
	 */
	
	@Test
	public void testUseLuckyDipCase2PositionMoreThan24() {
		// Setting up the index
		index = 2;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Setting position to 30
		player.setPosition(30);
		
		// Calling the output
		CardControl.useLuckyDip(index, playerStorage, player, input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "200 seeds were added to Michal's balance!\r\n"
				+ "Moved to PARAGUAY!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1700);
		assertEquals(player.getPosition(),24);
	}
	
	/**
	 * Testing the third case of the lucky dip with position less than 11
	 */
	
	@Test
	public void testUseLuckyDipCase3PositionLessThan11() {
		// Setting up the index
		index = 3;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		CardControl.useLuckyDip(index, playerStorage, player, input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to CHAD!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),11);
	}
	
	/**
	 * Testing the third case of the lucky dip with position more than 11
	 */
	
	@Test
	public void testUseLuckyDipCase3PositionMoreThan11() {
		// Setting up the index
		index = 3;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Setting position to 30
		player.setPosition(30);
		
		// Calling the output
		CardControl.useLuckyDip(index, playerStorage, player, input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "200 seeds were added to Michal's balance!\r\n"
				+ "Moved to CHAD!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1700);
		assertEquals(player.getPosition(),11);
	}

	/**
	 * Testing the fourth case of the lucky dip with position less than 12
	 */
	
	@Test
	public void testUseLuckyDipCase4PositionLessThan12() {
		// Setting up the index
		index = 4;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(0);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to Solar Power!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),12);
	}
	
	/**
	 * Testing the fourth case of the lucky dip with position between 12 and 28
	 */
	
	@Test
	public void testUseLuckyDipCase4PositionBetween12and28() {
		// Setting up the index
		index = 4;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(13);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to Hydro Power!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),28);
	}
	
	/**
	 * Testing the fourth case of the lucky dip with position more than 28
	 */
	
	@Test
	public void testUseLuckyDipCase4PositionMoreThan28() {
		// Setting up the index
		index = 4;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(29);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "200 seeds were added to Michal's balance!\r\n"
				+ "Moved to Solar Power!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),12);
	}
	
	/**
	 * Testing the fifth case of the lucky dip with position less than five
	 */
	
	@Test
	public void testUseLuckyDipCase5PositionLessThan5() {
		// Setting up the index
		index = 5;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(0);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to Cycle Path!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),5);
	}
	
	/**
	 * Testing the fifth case of the lucky dip with position between five and 15
	 */
	
	@Test
	public void testUseLuckyDipCase5PositionBetween5And15() {
		// Setting up the index
		index = 5;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(10);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to Boat Club!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),15);
	}
	
	/**
	 * Testing the fifth case of the lucky dip with position between 15 and 25
	 */
	
	@Test
	public void testUseLuckyDipCase5PositionBetween15And25() {
		// Setting up the index
		index = 5;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(20);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to Electric Car!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),25);
	}
	
	/**
	 * Testing the fifth case of the lucky dip with position between 25 and 35
	 */
	
	@Test
	public void testUseLuckyDipCase5PositionBetween25And35() {
		// Setting up the index
		index = 5;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(30);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to Paraglider!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),35);
	}
	
	/**
	 * Testing the fifth case of the lucky dip with position more than 35
	 */
	
	@Test
	public void testUseLuckyDipCase5PositionMoreThan35() {
		// Setting up the index
		index = 5;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(37);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "200 seeds were added to Michal's balance!\r\n"
				+ "Moved to Cycle Path!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),5);
	}
	
	/**
	 * Testing the sixth case of the lucky dip
	 */
	
	@Test
	public void testUseLuckyDipCase6() {
		// Setting up the index
		index = 6;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(0);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "50 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getBalance(),1550);
	}
	
	/**
	 * Testing the seventh case of the lucky dip
	 */
	
	@Test
	public void testUseLuckyDipCase7() {
		// Setting up the index
		index = 7;
		
		// Calling the lucky dip card
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Checking if the poachers trap key was set to true
		assertEquals(player.getKey(),true);
	}
	
	/**
	 * Testing the eighth case of the lucky dip
	 */
	
	@Test
	public void testUseLuckyDipCase8() {
		// Setting up the index
		index = 8;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(0);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Player moved back 3 spaces!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),36);
	}
	
	/**
	 * Testing the ninth case of the lucky dip
	 */
	
	@Test
	public void testUseLuckyDipCase9() {
		// Setting up the index
		index = 9;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(0);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to QUEENSLAND!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(player.getPosition(),39);
	}
	
	/**
	 * Testing the tenth case of the lucky dip
	 */
	
	@Test
	public void testUseLuckyDipCase10() {
		// Setting up the index
		index = 10;

		// Calling the method for the tenth case of the lucky dip cards
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Checking if the player is now in the poachers trap
		assertEquals(player.getPosition(),10);
	}
	
	/**
	 * Testing the eleventh case of the lucky dip
	 * !!!!!! IMPORTANT !!!!!!!!!!!
	 * Works when this test is ran by itself without interference of all 
	 * of the other tests
	 */
	
	@Test
	@Ignore
	public void testUseLuckyDipCase11() {
		// Setting up the index
		index = 11;
		
		// Adding a tree so it takes away 25 seeds
		ALocation sumatra = (ALocation) Board.getACard(3);
		sumatra.addTree();
		playerStorage.getPlayerList().get(1).buyLocation(3);
		playerStorage.getPlayerList().get(1).buyTree();

		// Calling the method for the tenth case of the lucky dip cards
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(1), input, rollNumber, playerIndex);

		// Checking if the player is now in the poachers trap
		assertEquals(playerStorage.getPlayerList().get(1).getBalance(),1475);
	}

	/**
	 * Testing the twelfth case of the lucky dip
	 */
	
	@Test
	public void testUseLuckyDipCase12() {
		// Setting up the index
		index = 12;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(0);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "50 seeds were taken from Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(playerStorage.getPlayerList().get(0).getBalance(),1450);
	}
	
	/**
	 * Testing the thirteenth case of the lucky dip with position less than 5
	 */
	
	@Test
	public void testUseLuckyDipCase13PositionLessThan5() {
		// Setting up the index
		index = 13;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(0);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "Moved to CYCLE PATH!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(playerStorage.getPlayerList().get(0).getPosition(),5);
	}
	
	/**
	 * Testing the thirteenth case of the lucky dip with position more than 5
	 */
	
	@Test
	public void testUseLuckyDipCase13PositionMoreThan5() {
		// Setting up the index
		index = 13;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(10);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "200 seeds were added to Michal's balance!\r\n"
				+ "Moved to CYCLE PATH!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(playerStorage.getPlayerList().get(0).getPosition(),5);
	}
	
	/**
	 * Testing the fourteenth case of the lucky dip
	 */
	
	@Test
	public void testUseLuckyDipCase14() {
		// Setting up the index
		index = 14;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(10);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "50 seeds given to all players!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(playerStorage.getPlayerList().get(0).getBalance(),1450);
		assertEquals(playerStorage.getPlayerList().get(1).getBalance(),1550);
	}
	
	
	/**
	 * Testing the fifteenth case of the lucky dip
	 */
	
	@Test
	public void testUseLuckyDipCase15() {
		// Setting up the index
		index = 15;

		// Setting up the output stream
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Calling the output
		playerStorage.getPlayerList().get(0).setPosition(10);
		CardControl.useLuckyDip(index, playerStorage, playerStorage.getPlayerList().get(0), input, rollNumber, playerIndex);

		// Testing if the output is correct
		String expectedOutput  = "20 seeds were added to Michal's balance!"; 

		assertEquals(expectedOutput,outContent.toString().trim());
		assertEquals(playerStorage.getPlayerList().get(0).getBalance(),1520);
	}
	

}
