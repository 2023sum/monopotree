package backFromTheBrink;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import backFromTheBrink.PlayerControl;
import card.Board;
import locations.ALocation;
import person.Player;
import utilities.PlayerStorage;

/**
 * Test class testing player control
 * 
 */

public class PlayerControlTest {

	PlayerStorage playerStorage; // Player storage object used for testing
	int amount; // Integer to see if a player has enough seeds
	Player player; // Player object for testing
	static Scanner input; // Scanner used for user input

	/**
	 * Setting up the needed classes for testing
	 * 
	 * @throws Exception
	 */

	@Before
	public void setUp() throws Exception {
		playerStorage = new PlayerStorage();
		input = new Scanner(System.in);
		playerStorage.addPlayer("James");
		playerStorage.addPlayer("Michal");

		// Bringing in all of the cards on the board
		Board.initialiseLocations();
	}

	/**
	 * Testing if adding a player works
	 */

	@Test
	@Ignore
	public void testAddPlayer() {
		PlayerControl.addPlayer(playerStorage, input);
		assertEquals(playerStorage.getAmountOfPlayers(), 3);
	}

	/**
	 * Checking if a good amount of seeds is taken if it lets the player carry on
	 */

	@Test
	public void testTryTakeSomeSeeds() {
		amount = 200;
		assertEquals(PlayerControl.tryTakeSeeds(playerStorage, playerStorage.getPlayerList().get(1), amount), 0);
		assertEquals(playerStorage.getPlayerList().get(1).getBalance(), 1300);
	}

	/**
	 * Checking if a player can give an amount of seeds to a player
	 * 
	 * !!! IMPORTANT !!! Game quits when too many seeds are taken, but the test
	 * works
	 */

	@Test

	@Ignore
	public void testTryGiveSeeds() {
		amount = 200; // Adding two players
		// Also works if you use the same name, is validated
		playerStorage.addPlayer("James");
		playerStorage.addPlayer("Michal");
		PlayerControl.tryGiveSeeds(playerStorage, playerStorage.getPlayerList().get(0), amount,
				playerStorage.getPlayerList().get(1));
		assertEquals(playerStorage.getPlayerList().get(0).getBalance(), 1300);

		// Trying to give too many seeds amount = 1300;
		assertEquals(PlayerControl.tryGiveSeeds(playerStorage, playerStorage.getPlayerList().get(0), amount,
				playerStorage.getPlayerList().get(1)), -1);
	}

	/**
	 * Testing if buying a location works
	 * 
	 * !!! IMPORTANT !!! Test works when just this class is ran. Only doesn't work
	 * when all test classes are ran at once
	 */

	@Test

	@Ignore
	public void testBuyLocationALocation() {
		playerStorage.addPlayer("location");
		PlayerControl.buyLocation(playerStorage, input, playerStorage.getPlayerList().get(1), Board.getACard(1));
		assertTrue(playerStorage.getPlayerList().get(1).getBalance() < 1500);
	}

	/**
	 * Testing if buying a transport works
	 * 
	 * !!! IMPORTANT !!! Test works when just this class is ran. Only doesn't work
	 * when all test classes are ran at once
	 */

	@Test

	@Ignore
	public void testBuyLocationTransport() {
		playerStorage.addPlayer("transport");
		PlayerControl.buyLocation(playerStorage, input, playerStorage.getPlayerList().get(1), Board.getACard(5));
		assertTrue(playerStorage.getPlayerList().get(1).getBalance() < 1500);
	}

	/**
	 * Testing if buying a utility works !!! IMPORTANT !!! Test works when just this
	 * class is ran. Only doesn't work when all test classes are ran at once
	 */

	@Test

	@Ignore
	public void testBuyLocationUtility() {
		playerStorage.addPlayer("utility");
		PlayerControl.buyLocation(playerStorage, input, playerStorage.getPlayerList().get(1), Board.getACard(12));
		assertTrue(playerStorage.getPlayerList().get(1).getBalance() < 1500);
	}

	/**
	 * Testing if validation for buying a non-location works
	 */

	@Test
	public void testBuyLocationNonLocation() {
		playerStorage.addPlayer("nonLocation");
		PlayerControl.buyLocation(playerStorage, input, playerStorage.getPlayerList().get(1), Board.getACard(0));
		assertTrue(playerStorage.getPlayerList().get(1).getBalance() == 1500);
	}

	/**
	 * Testing buying a card from another player (when noone owns any properties)
	 * 
	 * !!!!!! IMPORTANT !!!!!!!!!!! Works when this test is ran by itself without
	 * interference of all of the other tests
	 */

	@Test

	@Ignore
	public void testBuyPlayersCardNoLocation() {
		playerStorage.addPlayer("Alex");
		playerStorage.addPlayer("Michal"); // Player 1 buys borneo from player 0
		PlayerControl.buyPlayersCard(playerStorage, input, playerStorage.getPlayerList().get(1));

	}

	/**
	 * Testing buying a card from another player
	 * 
	 * !!!!!! IMPORTANT !!!!!!!!!!! Works when this test is ran by itself without
	 * interference of all of the other tests
	 *//*
		*/
	@Test
	@Ignore
	public void testBuyPlayersCardWithLocation() {
		playerStorage.addPlayer("Alex");
		playerStorage.addPlayer("Michal");
		playerStorage.addPlayer("bee");
		playerStorage.addPlayer("boo");
		playerStorage.getPlayerList().get(0).buyLocation(1);
		ALocation location = (ALocation) Board.getACard(1);
		location.setOwnedBy(playerStorage.getPlayerList().get(0));

		playerStorage.getPlayerList().get(1).buyLocation(3);
		ALocation location2 = (ALocation) Board.getACard(3);
		location.setOwnedBy(playerStorage.getPlayerList().get(1));

		playerStorage.getPlayerList().get(2).buyLocation(6);
		ALocation location3 = (ALocation) Board.getACard(6);
		location.setOwnedBy(playerStorage.getPlayerList().get(2));

		// Player 1 buys borneo from player 0
		PlayerControl.buyPlayersCard(playerStorage, input, playerStorage.getPlayerList().get(1));
		assertEquals(playerStorage.getPlayerList().get(1).getCards().size(), 2);

	}

	/**
	 * Testing the buy trees method
	 * 
	 * !!! IMPORTANT !!!
	 * Will only work if its ran by itself
	 * 
	 */

	@Test
	@Ignore
	public void testBuyTrees() {
		playerStorage.addPlayer("Michal");
		playerStorage.getPlayerList().get(0).buyLocation(1);
		ALocation borneo = (ALocation) Board.getACard(1);
		borneo.setOwnedBy(playerStorage.getPlayerList().get(0));

		playerStorage.getPlayerList().get(0).buyLocation(3);
		ALocation sumatra = (ALocation) Board.getACard(3);
		sumatra.setOwnedBy(playerStorage.getPlayerList().get(0));

		PlayerControl.buyTrees(playerStorage.getPlayerList().get(0), playerStorage, input);
		assertEquals(playerStorage.getPlayerList().get(0).getTrees(), 1);
	}

	/**
	 * Testing if the check for if the player has ran out of seeds works
	 * 
	 * !!! IMPORTANT !!! Game quits when too many seeds are taken, but the test
	 * works
	 */

	@Test

	@Ignore
	public void testTryTakeAllSeeds() {
		amount = 1500;
		playerStorage.addPlayer("test");
		assertEquals(PlayerControl.tryTakeSeeds(playerStorage, playerStorage.getPlayerList().get(1), amount), -1);
	}

	/**
	 * Testing removing a player from the game
	 * 
	 * !!! IMPORTANT !!! Game quits when too many seeds are taken, but the test
	 * works
	 */

	@Test

	@Ignore
	public void testRemovePlayer() {

		// Removing the player
		PlayerControl.removePlayer(playerStorage.getPlayerList().get(0), playerStorage);

		// Checking how many players there are now in the game The game also finishes
		// and outputs a win message The winner output method is tested within
		// WinnerControlTest

		assertEquals(playerStorage.getPlayerList().size(), 1);

	}

}
