package miscCards;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import card.Card;
import card.Board;

public class NonLocationTest {
	
	Card nonLocation; 	// The card declared for testing
	NonLocation PoacherTrap; 		// Location that will be tested
	ArrayList<Card> cards;  // List of all cards on the board

	@Before
	public void setUp() throws Exception {
		Board.initialiseLocations();
		cards = (ArrayList<Card>) Board.getCards();
		nonLocation = cards.get(10);
		PoacherTrap = (NonLocation) nonLocation;
	}

	@Test
	public void testGetTypeOfCard() {
		assertEquals(PoacherTrap.getTypeOfCard(), NonLocations.PoacherTrap);
	}



}
