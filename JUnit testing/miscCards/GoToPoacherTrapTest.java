package miscCards;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import person.Player;
import miscCards.GoToPoacherTrap;

public class GoToPoacherTrapTest {
	
	Player player;  //Player to be sent to poacher trap

	@Before
	public void setUp() throws Exception {
		player = new Player("Burnsy");
	}



	@Test
	public void testGoJail() {
		GoToPoacherTrap.goPoacherTrap(player, 4);
		assertEquals(player.getFreedom(), false);
		player.setKey(true);
		player.setFreedom(true);
		GoToPoacherTrap.goPoacherTrap(player, 4);
		assertEquals(player.getFreedom(), true);
	}

}
