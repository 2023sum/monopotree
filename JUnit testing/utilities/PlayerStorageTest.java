package utilities;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import person.Player;

/**
 * Player storage test class
 */

public class PlayerStorageTest {

	PlayerStorage storage; 			// The player storage object
	ArrayList<Player> playerList;   // Empty array list storing player objects
	String out;						// String to test correct output for showing game stats
	
	/**
	 * Creating an object of the player storage class
	 * Creating the empty arraylist of players to test object creation
	 * Creating a string of stats for a player object 
	 * @throws Exception
	 */
	
	@Before
	public void setUp() throws Exception {
		storage = new PlayerStorage();
		playerList = new ArrayList<Player>();
		out = "";
		out += "Player Name: " + "Michal" + "\n";
		out += "Player Seeds: " + 1500 + "\n";
		out += "Player Position: " + 0 + "\n";
	}
	
	/**
	 * Testing if getting the amount of players works
	 */

	@Test
	public void testGetAmountOfPlayers() {
		assertEquals(storage.getAmountOfPlayers(), 0);
	}
	
	/**
	 * Testing if getting the player list works
	 */
	
	@Test
	public void testGetPlayerList() {
		assertEquals(storage.getPlayerList(),playerList);
	}

	/**
	 * Testing adding a player
	 */
	
	@Test
	public void testAddPlayer() {
		storage.addPlayer("Michal");
		assertEquals(storage.getAmountOfPlayers(),1);
		// Testing if the name of the player in the list is correct
		assertEquals(storage.getPlayerList().get(0).getName(),"Michal");
	}
	
	/**
	 * Testing deleting a player
	 */

	@Test
	public void testDeletePlayer() {
		storage.addPlayer("Michal"); 								// Adding the player
		storage.deletePlayer(storage.getPlayerList().get(0));		// Removing the player
		assertEquals(storage.getAmountOfPlayers(),0);				// Checking if number of players has decremented
		assertEquals(storage.getPlayerList(),playerList);			// Checking if the list of players is now empty
	}
	
	/**
	 * Testing showing the game stats
	 */
	
	@Test
	public void testShowGameStats() {
		storage.addPlayer("Michal");
		assertEquals(storage.showGameStats(0),out);
	}

	/**
	 * Testing returning the position of the player
	 */
	
	@Test
	public void testReturnPlayerPosition() {
		storage.addPlayer("Michal");
		
		assertEquals(storage.returnPlayerPosition(storage.getPlayerList().get(0)),storage.getPlayerList().get(0).getPosition());
	}

}
