package utilities;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;


/**
 * The menu test class
 */

public class Menu_2Test {
	
	static Menu_2 menu;		// Menu object for testing
	static Scanner input;	// Scanner for user input
	
	/**
	 * Setting up the menu class
	 * @throws Exception
	 */

	@BeforeClass
	public static void setUp() throws Exception {
		String options[] = {"Play game","Quit game"};
		menu = new Menu_2("Game",options);
		input = new Scanner(System.in);
	}
	
	/**
	 * Testing getting an option from the user
	 * 
	 * !!! IMPORTANT !!!
	 * Ignored because of a conflict with the scanners when running all junit
	 * tests at once. If you run just this test class with it not ignored then
	 * it will pass.
	 * But if you run it with the rest it'll get a scanner has no line exception
	 * due to multiple System.in input streams
	 */

	@Test
	@Ignore
	public void testGetOptionChoice() {
		// Using the input of one
		assertEquals(menu.getOptionChoice(input),1);
		// Using input of non integer
		// Just loops back through until you choose zero
		assertEquals(menu.getOptionChoice(input),0);
	}

	/**
	 * Testing the print method
	 * 
	 * !!! IMPORTANT !!!
	 * The output is right but the test won't pass
	 */
	
	@Test
	@Ignore
	public void testPrint() {
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));

	    menu.print();

	    String expectedOutput  = "~~~~\nWelcome to Game\n~~~~\n\n1 ---> Play game\n2 ---> Quit game"; 
	    assertEquals(expectedOutput,outContent.toString().trim());
		
	}
	
	/**
	 * Closing the scanner at the end of the test
	 */
	
	@After 
	public void tearDown() {
		input.close();
	}

}
