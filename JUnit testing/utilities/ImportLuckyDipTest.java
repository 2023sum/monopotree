package utilities;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Testing if the import lucky dip cards class works correctly
 *
 */

public class ImportLuckyDipTest {
	
	String[] array; 		// Array of the lucky dip cards

	/**
	 * Importing the lucky dip cards from the file
	 * @throws Exception
	 */
	
	@Before
	public void setUp() throws Exception {
		array = ImportLuckyDip.importCards();
	}
	
	/**
	 * Testing that all of the cards were imported correctly.
	 */

	@Test
	public void testImportCards() {
		// Testing the first element
		assertEquals("Advance to \"GARDEN CENTRE\". Collect 200 seeds.",array[0]);
		
		// Testing the middle of the array
		assertEquals("Bank pays you a green deal grant of 50 seeds.",array[6]);
		
		// Testing the last card
		assertEquals("You have been nominated for eco-friendly person of the year. Collect 20 seeds.",array[15]);
	}

}
