package utilities;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Testing the import treasure chest class
 */

public class ImportTreasureChestTest {
	
	String[] array; 	// Array of the treasure chest cards
	
	/**
	 * Importing the treasure chest cards from the file
	 * @throws Exception
	 */
	
	@Before
	public void setUp() throws Exception {
		array = ImportTreasureChest.importCards();
	}

	/**
	 * Testing that all of the cards are imported correctly
	 */
	
	@Test
	public void testImportCards() {
		// Testing the first element which is meant to be in the array
		assertEquals("Advance to \"GARDEN CENTRE\". Collect 200 seeds.",array[0]);
		
		// Testing the middle of the cards
		assertEquals("Happy birthday! Collect 10 seeds from every player.",array[6]);
		
		// Testing the last card
		assertEquals("Forestry insurance! Pay 10 seeds for every owned tree and 20 seeds for every owned forest.",array[15]);
	}

}
