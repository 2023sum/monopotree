package person;

import card.Card;
import card.Board;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

/**
 * Testing the player class
 *
 */
public class PlayerTest {

	// Player object and it's cards
	// Board object for all of the cards on the board
	public Player player;
	public Player player2;
	public Board board;
	public ArrayList<Card> cards = new ArrayList<Card>();
	
	
	/**
	 * Constructing the object to use in tests
	 */
	@Before
	public void setUp() throws Exception
	{
		String name = "Michal";
		this.player = new Player(name);
		String name2 = "James";
		this.player2 = new Player(name2);
		Board.initialiseLocations();
	}
	
	/**
	 * Testing object construction and getters
	 */
	
	@Test
	public void testPlayerGetters() {
		assertEquals(player.getName(),"Michal");
		assertEquals(player.getBalance(),1500);
		assertEquals(player.getPosition(),0);
		assertEquals(player.getFreedom(),true);
		assertEquals(player.getCards(),cards);
		assertEquals(player.getPoacherTrapEntryNum(),0);
		assertEquals(player.getTrees(),0);
		assertEquals(player.getKey(),false);
	}

	/**
	 * Testing setting the poacher trap entry number
	 */
	@Test
	public void testSetPoacherTrapEntryNum() {
		player.setPoacherTrapEntryNum(2);
		assertEquals(player.getPoacherTrapEntryNum(),2);
	}
	
	/**
	 * Testing setting the position of the player
	 */
	@Test
	public void testSetPosition() {
		player.setPosition(5);
		assertEquals(player.getPosition(),5);
	}

	
	/**
	 * Testing taking seeds away from the player
	 */
	@Test
	public void testTakeSeeds() {
		int seeds = 1500 - 1300;
		player.takeSeeds(1300);
		assertEquals(player.getBalance(),seeds);
	}
	
	/**
	 * Testing taking too many seeds away from a player
	 */
	@Test
	public void testTakingTooManySeeds() {
		player.takeSeeds(1700);
		assertEquals(player.getBalance(),1500);
	}
	
	/**
	 * Testing giving seeds to another player
	 */

	@Test
	public void testGiveSeedsToPlayer() {
		player.giveSeedsToPlayer(200,player2);
		assertEquals(player.getBalance(),1300);
		assertEquals(player2.getBalance(),1700);
	}
	
	/**
	 * Testing adding seeds to a player
	 */

	@Test
	public void testAddSeeds() {
		player.addSeeds(200);
		assertEquals(player.getBalance(),1700);
	}
	
	/**
	 * Testing to see if rolling works
	 */

	@Test
	public void testRoll() {
		int roll = player.roll();
		assertEquals(player.getPosition(),roll);
	}
	
	/**
	 * Testing to see if moving to a certain position works
	 */

	@Test
	public void testMove() {
		player.move(5);
		assertEquals(player.getPosition(),5);
	}
	
	/**
	 * Testing to see if rolling with no position change works
	 */

	@Test
	public void testRollNoPosChange() {
		  int roll = player.rollNoPosChange();
		  int high = 6;
		  int low = 1;
		  assertTrue("Error, roll is too high", high >= roll);
		  assertTrue("Error, roll is too low",  low  <= roll);
	}
	
	/**
	 * Testing to see if checking to see if you can buy a location works
	 */

	@Test
	public void testBuyLocation() {
		// Treasure chest
		assertEquals(player.buyLocation(2),true);
		// Lucky dip
		assertEquals(player.buyLocation(36),true);
		// Location
		assertEquals(player.buyLocation(1),false);
		// Non - location
		assertEquals(player.buyLocation(20),true);
		// Tariff
		assertEquals(player.buyLocation(4),true);
		// Utility
		assertEquals(player.buyLocation(12),false);
		// Train station
		assertEquals(player.buyLocation(5),false);
		
	}
	


	/**
	 * Testing if checking if the player owns a location works
	 */
	
	@Test
	public void testIfOwned() {
		// Testing for a non owned location
		assertEquals(player.ifOwned(Board.getCards().get(1)),false);
		player.buyLocation(9);
		// Testing for a bought location
		assertEquals(player.ifOwned(Board.getCards().get(9)),true);
	}
	
	/**
	 * Testing buying a location from another player
	 */
	
	@Test
	public void testBuyLocationPlayer() {
		// player buying a location from player 2 for 200 seeds
		player2.buyLocation(3);
		player.buyLocationPlayer(Board.getACard(3),200);
		assertEquals(player.getBalance(),1300);
		assertEquals(player.ifOwned(Board.getACard(3)),true);
		player2.sellLocation(Board.getACard(3), 200);
		assertEquals(player2.ifOwned(Board.getACard(3)),false);
	}
	
	/**
	 * Testing if selling a location works
	 */
	
	@Test
	public void testSellLocation() {
		player.sellLocation(Board.getACard(9),200);
		
		// Checking if the right amount of seeds has been deposited and the card has
		// been removed from the arraylist
		assertEquals(player.getBalance(),1700);
		assertEquals(player.getCards().size(),0);
	}

	/**
	 * Testing if setting the freedom attribute works
	 */
	
	@Test
	public void testSetFreedom() {
		player.setFreedom(false);
		assertEquals(player.getFreedom(),false);
	}

	/**
	 * Testing setting the key
	*/
	
	@Test
	public void testSetKey() {
		player.setKey(true);
		assertEquals(player.getKey(),true);
	}

	/**
	 * Testing if the player can buy a tree
	*/
	
	@Test
	public void testBuyTree() {
		player.buyTree();
		assertEquals(player.getTrees(),1);
	}

}