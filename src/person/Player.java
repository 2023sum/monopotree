package person;

import java.util.ArrayList;

import card.Board;
import card.Card;
import locations.ALocation;
import locations.Transport;
import locations.Utility;

/**
 * Player class which controls the user in the game
 *
 */

public class Player{

	private int position; // The position of the player
	private int seeds;	  // How many seeds the player has
	private boolean freedom; // If the player is in poachers trap or not
	private boolean key; // Key to get out of poachers trap
	private ArrayList<Card> cards; // Cards that the player owns
	private int trees; // Trees that the player owns
	private String name; // Name of the player
	private int poacherTrapEntryNum; // Number of entry to the poachers trap

	/**
	 * Constructor for the player class
	 * 
	 * @param name - Name of the player
	 */

	public Player(String name) {
		this.seeds = 1500;
		this.position = 0;
		this.name = name;
		this.freedom = true;
		this.cards = new ArrayList<Card>();
		this.poacherTrapEntryNum = 0;
		this.trees = 0;
		this.key = false;
	}

	/**
	 * overloaded constructor
	 * see above for definitions
	 * @param position
	 * @param seeds
	 * @param freedom
	 * @param key
	 * @param cards
	 * @param trees
	 * @param name
	 * @param poacherTrapEntryNum
	 */
	public Player(String name, int position, int seeds, boolean freedom, boolean key, int poacherTrapEntryNum, int trees, ArrayList<Card> cards) {
		super();
		this.position = position;
		this.seeds = seeds;
		this.freedom = freedom;
		this.key = key;
		this.cards = cards;
		this.trees = trees;
		this.name = name;
		this.poacherTrapEntryNum = poacherTrapEntryNum;
	}

	/**
	 * Getter for the entry to poachers trap
	 * 
	 * @return - poachers trap entry number
	 */

	public int getPoacherTrapEntryNum() {
		return this.poacherTrapEntryNum;
	}

	/**
	 * Setting the poachers trap entry number
	 * 
	 * @param num - The poachers trap entry number
	 */

	public void setPoacherTrapEntryNum(int num) {
		this.poacherTrapEntryNum = num;
	}

	/**
	 * Getting the position of the player
	 * 
	 * @return - Players position
	 */

	public int getPosition() {
		return this.position;
	}

	/**
	 * Setting the position of the player
	 * 
	 * @param pos - Players position
	 */

	public void setPosition(int pos) {
		this.position = pos;
	}

	/**
	 * Getting the players cards
	 * 
	 * @return - cards that the player owns
	 */

	public ArrayList<Card> getCards() {
		return cards;
	}

	/**
	 * Getting the players balance
	 * 
	 * @return - The amount of seeds that the player has
	 */

	public int getBalance() {
		return this.seeds;
	}

	/**
	 * Taking seeds away from the player
	 * 
	 * @param seeds - The amount of seeds that the player has
	 * @return - boolean if taking the seeds away was succesful.
	 */

	public boolean takeSeeds(int seeds) {
		if (this.seeds > seeds) {
			this.seeds -= seeds;
			return true;
		}
		return false;
	}

	/**
	 * Giving seeds to another player
	 * 
	 * @param seeds  - how many seeds to be transferred
	 * @param player - which player to give the seeds to
	 */

	public boolean giveSeedsToPlayer(int seeds, Player player) {
		if (takeSeeds(seeds)) {
			player.addSeeds(seeds);
			return true;
		} else {
			int balance = this.getBalance();
			takeSeeds(balance);
			player.addSeeds(balance);
			return false;
		}
	}

	/**
	 * Adding seeds to a player
	 * 
	 * @param seeds - Amount of seeds to be added
	 */

	public void addSeeds(int seeds) {
		this.seeds += seeds;
	}

	/**
	 * Rolling the dice
	 * 
	 * @return - The dice roll
	 */

	public int roll() {
		// Rolling dice
		int roll = (int) (Math.random() * 6 + 1);

		// Moving the player
		this.position += roll;
		if (this.position > 39) {
			this.position = this.position - 39;
		}
		return roll;

	}

	/**
	 * Moving the player after a roll
	 * 
	 * @param roll - how much the player rolled
	 */

	public void move(int roll) {

		// Moving the player
		this.position += roll;
		if (this.position > 39) {
			this.position = this.position - 40;
		}
	}

	/**
	 * Rolling but not moving the player
	 * 
	 * @return - The amount rolled
	 */

	public int rollNoPosChange() {
		// Rolling dice
		int roll = (int) (Math.random() * 6 + 1);
		return roll;
	}

	/**
	 * Buying a location
	 * 
	 * @param i - the position of the location on the board
	 * @return - if the location was bought or not
	 */

	public boolean buyLocation(int pos) {	
		
		if (Board.getCards().get(pos).getObtainable() == false) {
			Card cardToBuy = Board.getCards().get(pos);
            cards.add(cardToBuy);
			Board.getCards().get(pos).setObtainable(true);
			
			return false;
		} else {
			return true;
		}

	}

	/**
	 * Returns a true or false if the player owns this location or not
	 * 
	 * @param location - The location in question
	 * @return - Boolean of if the player owns the location
	 */

	public boolean ifOwned(Card location) {
		for (int i = 0; i < this.cards.size(); i++) {
			if (location.equals(cards.get(i))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Buys a location off of a player
	 * @return false if the player is now out of seeds
	 * 
	 * @param card - the location in question
	 * @param j    - how much the location costs
	 */

	public boolean buyLocationPlayer(Card card, int cost) {
		if (card.getObtainable() == true) {
			cards.add(card);
			if (this.takeSeeds(cost)) {
				return true;
			}
			else {
				return false;
			}
		} 
		else {
			System.out.println("This card is not owned by a player");

		}
		return true;

	}

	/**
	 * Selling a location
	 * 
	 * @param card - The location in question
	 * @param j    - How much the location costs
	 */

	public void sellLocation(Card card, int cost) {
		cards.remove(card);
		this.addSeeds(cost);

	}

	/**
	 * Setting the freedom attribute of the player
	 * 
	 * @param freedom - if the player is in the poachers trap or not
	 */

	public void setFreedom(boolean freedom) {
		this.freedom = freedom;
	}

	/**
	 * Getting the freedom attribute
	 * 
	 * @return - the freedom attribute
	 */

	public boolean getFreedom() {
		return this.freedom;
	}

	/**
	 * Setting the key attribute
	 * 
	 * @param key - the key attribute
	 */

	public void setKey(boolean key) {
		this.key = key;
	}

	public boolean getKey() {
		return this.key;
	}

	/**
	 * Returns the name attribute
	 * 
	 * @return - name of the player
	 */

	public String getName() {
		return this.name;
	}

	/**
	 * Allows the user to buy a tree
	 * 
	 * @return - Boolean if the user can buy the tree or not
	 */

	public boolean buyTree() {
		if (trees < 5) {
			this.trees++;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the amount of trees
	 * 
	 * @return
	 */

	public int getTrees() {
		return this.trees;
	}

}
