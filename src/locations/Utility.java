package locations;

import java.util.ArrayList;

import card.Card;
import person.Player;

public class Utility extends Locations {
	
	private int cost;					// The cost of the utility
	private int donation; 				// How much the donation is on this utility
	private Player ownedBy;				// Who the utility is owned by
	private boolean owned;				// If the utility is owned or not
	
	
	/**
	 * The utility constructor
	 * @param name - The name of the utility
	 * @param position - The position of the utility on the board
	 * @param obtainable - If the utility is obtainable or not
	 * @param cost - The price of the utility
	 * @param canBuy - If the utility can be bought or not 
	 * @param ownedBy - Who the utility is owned by
	 */
	
	public Utility(String name, int position, boolean obtainable, int cost, boolean canBuy, Player ownedBy,String fact)
	{
		super(name,obtainable,canBuy,false,position,cost,ownedBy,fact);
		this.cost = cost;
		this.donation = 4;
		this.ownedBy = ownedBy;
	}
	
	/**
	 * Takes the donation for the utility
	 * @param roll - What the player who landed on the utility rolled
	 * @param player - the player that landed on the utility
	 * @param cards - The cards on the board
	 * @return - If taking the donation was successful or not, false if the player runs out of seeds
	 */

	public int takeDonation(int roll, Player player)
	{
		Player Owner = this.ownedBy;
		ArrayList<Card> cards = Owner.getCards();
		int count = 0;
		for (Card name : cards) {
			if (name.getName().equals("Solar Power") || name.getName().equals("Hydro Power")) {
				count++;
			}
		}
		
		int toPay;
		
		switch (count) {
		case 1:
			toPay = 4 * roll;

			player.giveSeedsToPlayer(toPay, Owner);
			

			if (!player.giveSeedsToPlayer(toPay, Owner)) {
				return 0;
			}
			return toPay;

		case 2:
			toPay = 10 * roll;

			player.giveSeedsToPlayer(toPay, Owner);
			

			if (!player.giveSeedsToPlayer(toPay, Owner)) {
				return 0;
			}
			return toPay;


		default:
			return 0;
		}
	}
	
	/**
	 * Takes the donation after landing on this card due to a lucky dip
	 * @param roll - how much the player rolled
	 * @param player - the player that landed
	 */
	

	

	public int takeDonationLuckyDip(int roll, Player player)

	{
		Player Owner = this.ownedBy;
		int toPay;
		toPay = 10 * roll;

		player.giveSeedsToPlayer(toPay, Owner);	
		

		if (!player.giveSeedsToPlayer(toPay, Owner)) {
			return 0;
		}	
		return toPay;

	}
	
	/**
	 * Returns the cost of the utility
	 * @return - cost
	 */
	
	public int getCost() {
		return cost;
	}
	
	/**
	 * Sets who the class is own by
	 */
	@Override
	public void setOwnedBy(Player player)
	{
		this.ownedBy = player;
	}

}
