package locations;

import java.rmi.UnexpectedException;
import java.util.ArrayList;

import person.Player;
import card.Board;
import card.Card;

public class Transport extends Locations {

	private int donation1; // Donation with one transport owned
	private int donation2; // Donation with two transports owned
	private int donation3; // Donation with three transports owned
	private int donation4; // Donation with four transports owned

	/**
	 * Constructor to instantiate a transport class
	 * 
	 * @param name       - Name of the card
	 * @param cost       - How much the card costs
	 * @param donation1      - Donation with one transport owned
	 * @param donation2      - Donation with two transports owned
	 * @param donation3      - Donation with three transports owned
	 * @param donation4      - Donation with four transports owned
	 * @param owned      - Boolean if someone owns this card or not
	 * @param position   - The position of this card on the board
	 * @param ownedBy    - Which player owns this card
	 * @param obtainable - If you can buy this card or not
	 */

	public Transport(String name, int cost, int donation1, int donation2, int donation3, int donation4, boolean owned, int position,
			Player ownedBy, boolean obtainable, boolean canBuy,String fact) {
		super(name, obtainable, canBuy, owned, position, cost, ownedBy,fact);
		this.donation1 = donation1;
		this.donation2 = donation2;
		this.donation3 = donation3;
		this.donation4 = donation4;

	}
	
	/**
	 * Taking donation from a player who lands on this card
	 * @return false if the player runs out of seeds
	 * @param player - The player who landed on the card
	 * @param cards - The array of cards
	 * @return - If taking the seeds away was successful or not
	 */

	public int takeDonation(Player player) {
		Player Owner = this.ownedBy;
		ArrayList<Card> cards = Owner.getCards();
		int count = 0;
		for (Card name : cards) {
			if (name.getName().equals("Cycle Path") || name.getName().equals("Boat Club")
					|| name.getName().equals("Electric Car") || name.getName().equals("Electric Car")
					|| name.getName().equals("Paraglider")) {
				count++;
			}
		}

		switch (count) {
		case 1:
			player.giveSeedsToPlayer(this.donation1, Owner);
			return this.donation1;
		case 2:
			player.giveSeedsToPlayer(this.donation2, Owner);
			return this.donation2;
		case 3:
			player.giveSeedsToPlayer(this.donation3, Owner);
	
			return this.donation3;
		case 4:
			player.giveSeedsToPlayer(this.donation4, Owner);
			return this.donation4;
		default:
			return 0;
		}
	}
	
	/**
	 * 
	 * @param player
	 * @param cards
	 * @return false if the player runs out of seeds
	 */
	
	public int takeDonationLuckyDip(Player player) {
		Player Owner = this.ownedBy;
		ArrayList<Card> cards = Owner.getCards();
		int count = 0;
		for (Card name : cards) {
			if (name.getName().equals("Cycle Path") || name.getName().equals("Boat Club")
					|| name.getName().equals("Electric Car") || name.getName().equals("Electric Car")
					|| name.getName().equals("Paraglider")) {
				count++;
			}
		}

		switch (count) {
		case 1:
			player.giveSeedsToPlayer(2 * this.donation1, Owner);
	
			return 2 * this.donation1;
		case 2:
			player.giveSeedsToPlayer(2 * this.donation2, Owner);
			
			return 2 * this.donation2;
		case 3:
			player.giveSeedsToPlayer(2 * this.donation3, Owner);
			
			return 2 * this.donation3;
		case 4:
			player.giveSeedsToPlayer(2 * this.donation4, Owner);
			
			return 2 * this.donation4;
		default:
			return 0;
		}
	}

	/**
	 * Gets the current amount of donation for this transport
	 * @return the current donation for the given transport
	 * @throws UnexpectedException when the current transport isn't owned
	 */
	
	public int getCurrentDonation(){
		int donation = 0;

		Transport tempTransport = (Transport) Board.getACard(5);
		if (this.ownedBy == tempTransport.ownedBy) {
			donation += 1;
		}

		tempTransport = (Transport) Board.getACard(15);
		if (this.ownedBy == tempTransport.ownedBy) {
			donation += 1;
		}

		tempTransport = (Transport) Board.getACard(25);
		if (this.ownedBy == tempTransport.ownedBy) {
			donation += 1;
		}

		tempTransport = (Transport) Board.getACard(35);
		if (this.ownedBy == tempTransport.ownedBy) {
			donation += 1;
		}

		switch (donation) {
		case (1):
			return this.donation1;
		case (2):
			return this.donation2;
		case (3):
			return this.donation3;
		case (4):
			return this.donation4;
		default:
			return -1;
		}
	}
	
	/**
	 * Sets who the class is own by
	 */
	@Override
	public void setOwnedBy(Player player)
	{
		this.ownedBy = player;
	}

}
