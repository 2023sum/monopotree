package locations;

import person.Player;



public class ALocation extends Locations{
	
	private int noOfTrees;
	
	boolean forest;						// Sets to true once someone puts a forest on this location
	
	private int donation0;			// Donation with no trees
	private int donation1;			// Donation with one tree
	private int donation2;			// Donation with two trees
	private int donation3;			// Donation with three trees
	private int donation4;			// Donation with four trees
	private int donationForest;		// Donation with a forest
	
	private int treePrice;		// Price of tree
	private Player ownedBy; 	// Who this location is owned by
	private String colour;		// Colour of the card
	
	/**
	 * Constructor to set all of the attributes for each location
	 * @param name - Name of the location
	 * @param colour - Colour of the card
	 * @param cost - Cost of the location
	 * @param treePrice - Price of a tree
	 * @param donation0 - Donation with no trees
	 * @param donation1 - Donation with one tree
	 * @param donation2 - Donation with two trees
	 * @param donation3 - Donation with three trees
	 * @param donation4 - Donation with four trees
	 * @param donationForest - Donation with a forest
	 * @param owned - If it's owned or not 
	 * @param position - Position of this location on the board
	 * @param ownedBy - Who the location is owned by
	 * @param obtainable - If the location is obtainable or not
	 */
	
	public ALocation(String name, String colour, int cost, int treePrice, int donation0, int donation1, int donation2, int donation3, int donation4, int donationForest, boolean owned, int position, Player ownedBy, boolean obtainable, boolean canBuy, String fact)
	{
		super(name,obtainable,canBuy,owned,position,cost,ownedBy,fact);
		this.donation0 = donation0;
		this.donation1 = donation1;
		this.donation2 = donation2;
		this.donation3 = donation3;
		this.donation4 = donation4;
		this.donationForest = donationForest;
		
		this.owned = false;
		this.position = position;
		this.obtainable = false;
		this.colour = colour;
		this.treePrice = treePrice;
		this.canBuy = true;
		this.forest = false;
	}
	
	/**
	 * Takes donation for this location
	 * @return false if the player runs out of seeds
	 */
	
	public int takeDonation(Player player)
	{
		
		if (this.forest == true)
		{
			player.giveSeedsToPlayer(this.donationForest, this.ownedBy);
			
			return this.donationForest;

		}
		else
		{
			switch(this.noOfTrees)
			{
			case 0:

				player.giveSeedsToPlayer(this.donation0, this.ownedBy);
				
				return this.donation0;

			case 1:

				player.giveSeedsToPlayer(this.donation1, this.ownedBy);
				
				return this.donation1;

			case 2:

				player.giveSeedsToPlayer(this.donation2, this.ownedBy);

				return this.donation2;

			case 3:
				player.giveSeedsToPlayer(this.donation3, this.ownedBy);

				return this.donation3;
			case 4:

				player.giveSeedsToPlayer(this.donation4, this.ownedBy);
				
				return this.donation4;

			default:
				return 0;
			}
		}
		
	}
	
	/**
	 * Adds a tree onto this location
	 * @return
	 */
	
	public String addTree()
	{
		if(this.noOfTrees < 5)
		{
			this.noOfTrees++;
		}
		else
		{
			return "You can't add a tree here anymore";
		}
		
		if(this.noOfTrees == 5)
		{
			this.forest = true;
			return "You now have a forest";
		}
		return "You have added a tree";
		
	}
	
	/**
	 * Seeing if the location is bought
	 * @return - boolean if the location is bought
	 */

	public boolean getOwned() {
		return owned;
	}
	
	/**
	 * Seeing how much the current donation for this location is
	 * @return - integer of how many seeds the donation is
	 */

	public int getCurrentDonation() {
		if(this.forest) {
			return this.donationForest;
		}
		switch(this.noOfTrees) {
		case(0):
			return this.donation0;
		case(1):
			return this.donation1;
		case(2):
			return this.donation2;
		case(3):
			return this.donation3;
		case(4):
			return this.donation4;
		default:
			return -1;
		}
	}
	
	/**
	 * Gets the price of a tree
	 * @return - The price of a single tree
	 */
	
	public int getTreePrice()
	{
		return this.treePrice;
	}
	
	/**
	 * Gets the amount of trees on this location
	 * @return - The amount of trees on this location
	 */
	
	public int getNoOfTrees()
	{
		return this.noOfTrees;
	}
	
	
	public String getColour() {
			return colour;
	}
	
	public Player getOwnedBy() {
			return ownedBy;
	}
	

	
	/**
	 * sets the number of trees on ALocation when importing from a save file
	 * @param noOfTrees number of trees from file
	 */
	public void SetNoOfTrees(int TreeNum) {
		if(TreeNum > 5) {
			TreeNum = 5;
		}
		this.noOfTrees = TreeNum;
	}
	
	/**
	 * Sets who the class is own by
	 */
	@Override
	public void setOwnedBy(Player player)
	{
		this.ownedBy = player;
	}

	
}
