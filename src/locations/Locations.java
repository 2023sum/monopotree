package locations;

import card.Card;
import person.Player;



public abstract class Locations extends Card{
	
	protected boolean owned;
	protected int position;
	protected int price;
	protected String fact;
	protected Player ownedBy;
	
	/**
	 * Locations constructor
	 * @param name - Name of the location
	 * @param obtainable - If the location is obtainable or not
	 * @param owned - If the location is owned or not
	 * @param position - Position of the location on the board
	 * @param price - Price of the location
	 * @param ownedBy - Who the location is owned by
	 */
	
	public Locations(String name, boolean obtainable, boolean canBuy, boolean owned, int position, int price, Player ownedBy, String fact)
	{
		super(name,obtainable,canBuy);
		this.owned = owned;
		this.price = price;
		this.position = position;
		this.ownedBy = null;
		this.fact = fact;
		
		
	}
	

	/**
	 * Taking a location from a player
	 * @param player - The player who the location is being taken from
	 * @return - Boolean if it went through or not
	 */
	
	public boolean takeLocation(Player player) 
	{
		if(ownedBy == null)
		{
			return false;
		}
		else
		{
		ownedBy = null;
		return true;
		}
	}
	
	public int getPrice() {
		return price;
	}
	
	
	/**
	 * Sets who the class is own by
	 */
	
	public void setOwnedBy(Player player)
	{
		this.ownedBy = player;
		this.owned = true;
	}
	
	/**
	 * Sets who the class is own by
	 */
	
	public Player getOwnedBy()
	{
		return this.ownedBy;
	}
	
	
	public String getFact()
	{
		return this.fact;
	}
	

}
