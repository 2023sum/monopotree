
package utilities;
import java.util.ArrayList;

import person.Player;

/**
 * Banker class which is basically the administrator of the game
 *
 */

public class PlayerStorage{
	
	ArrayList<Player> playerList;		// ArrayList of players in the game
	static private int playerCount;		// Amount of players in the game
	
	/**
	 * Constructor to declare a player storage object
	 */
	
	public PlayerStorage()
	{
		// No players at the start of the game
		playerList = new ArrayList<Player>();
		playerCount = 0;
	}
	
	/**
	 * Returns how many players there are in the game
	 * @return - amount of players in the game
	 */
	
	public int getAmountOfPlayers()
	{
		return playerCount;
	}
	
	/**
	 * Returns the list of players in the game
	 * @return - list of players
	 */
	
	public ArrayList<Player> getPlayerList()
	{
		return playerList;
	}
	
	/**
	 * Adds a player to the game
	 * @param playerName - name of the player
	 */
	
	public void addPlayer(String playerName)
	{
		Player player = new Player(playerName);
		playerCount++;
		playerList.add(player);
	}
	
	/**
	 * adds an imported player to the game
	 */
	public void addImportPlayer(Player iPlayer) {
		playerCount++;
		playerList.add(iPlayer);
	}
	
	/**
	 * Removes a player from the game
	 * @param player - player in question
	 * @return - if the removal was successful or not
	 */
	
	public boolean deletePlayer(Player player)
	{
		for(int i = 0; i < playerList.size(); i++)
		{
			Player playeri = playerList.get(i);
			if(playeri.equals(player))
			{
				playerList.remove(i);
				playerCount--;
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Shows the stats of the game
	 * @param i - the player index
	 * @return - the string to be output
	 */
	
	public String showGameStats(int player)
	{
		String out =  "";
			
				out += "Player Name: " + playerList.get(player).getName() + "\n";
				out += "Player Seeds: " + playerList.get(player).getBalance() + "\n";
				out += "Player Position: " + playerList.get(player).getPosition() + "\n";
				
			return out;
	}
	
	/**
	 * Returns the position of the player in the player list 
	 * @param player - player in question
	 * @return - the index of the player in the player list
	 */
	
	public int returnPlayerPosition(Player player)
	{
		int count = 0;
		for(int i = 0; i < playerList.size(); i++)
		{
			if(playerList.get(i).equals(player))
			{
			return count;
			}
			else {
				count++;
			}
		}
		return -1;
	}
	
}

