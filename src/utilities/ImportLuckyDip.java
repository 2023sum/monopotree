package utilities;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * 
 * @author Kelsey Forsythe
 * Class for importing the lucky dip cards
 * 
 */

public class ImportLuckyDip {
	
	/**
	 * Importing the lucky dip cards
	 * @return - an array with all of the lucky dip cards
	 */
	
	public static String[] importCards() 
	{
		int i = 0;
		String filePath = "src/files/LuckyDip.txt";
		String[] myArray = new String[16];
		
		try {
			
			File txtFile = new File(filePath);
			
			Scanner myScanner = new Scanner(txtFile);
			
			while(myScanner.hasNextLine()) {
				String data = myScanner.nextLine();
				myArray[i] = data;
				i++;
			}
			
			myScanner.close();
		}
			
			catch (FileNotFoundException e) {
			}
		
		return myArray;
	}
}
