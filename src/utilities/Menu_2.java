package utilities;
import java.util.Scanner;

/**
 * The menu class for outputting data as a menu in the game class
 *
 */

public class Menu_2 {
	
	private String options[];			// Options the user can choose from
	private String name; 				// The menu title
	
	/**
	 * Constructor for the menu class
	 * @param name - Name of the menu
	 * @param options - options in the menu
	 */
	
	public Menu_2(String name, String options[]) {
		this.name = name;
		copyOptions(options);
	}

	/**
	 * Gets the choice from the user
	 * @param input - the scanner from the main method
	 * @return - If the user picked a correct option or not
	 */
	
	public int getOptionChoice(Scanner input) {
		
		boolean check = false;
		int choice = 0;
		// Asking what the user would like to choose
		do {
			try {
				System.out.print("\nEnter choice: ");
				choice = input.nextInt();
				input.nextLine();
				check = true;
			// Error handling if the user doesn't input the right value
			} catch (Exception e) {
				System.out.println("\nInvalid option\nPlease try again\n");
				input.nextLine();
				check = false;
			}
		} while (check == false);
		return choice;		
	}
	
	/**
	 * Puts the options in the array in this class
	 * @param array - the options for the menu
	 */
	
	private void copyOptions(String array[]) {
		if ( array != null) {
			options = new String[array.length];
			for(int i=0;i<array.length;i++) {
				options[i] = array[i];
			}
		}
		else {
			options = null;
		}
	}
	
	/**
	 * Outputs the menu
	 */
	
	public void print() {
		if (name == null && options == null) {
		
			for(int i=0;i<17;i++) {
				System.out.print("~");
			}
			System.out.print("\r\n+ No Menu Found +\r\n");
			for(int i=0;i<17;i++) {
				System.out.print("~");
			}
		}
		if (name != null && options != null) {
			for(int i=0;i<name.length();i++) {
				System.out.print("~");
			}
			System.out.println(name);
			for(int i=0;i<name.length();i++) {
				System.out.print("~");
			}
			System.out.println("\n");
			int num = 1;
			for(String str : options) {
				System.out.println(num+" ---> "+str);
				num++;
			}
			System.out.println();
		}
			
	}
}
