package utilities;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import card.Card;
import locations.ALocation;
import person.Player;
import utilities.PlayerStorage;

public class ExportGame {

	/**
	 * 
	 * @param pStore     the current list of players
	 * @param turnNumber the turn number of the game
	 * @return true if the save was successful
	 */
	public static boolean SaveGame(PlayerStorage pStore, int turnNumber) {

		LocalDateTime dateTime = LocalDateTime.now();
		DateTimeFormatter formatTime = DateTimeFormatter.ofPattern("dd-MM-yy-HH-mm");
		String formatDateTime = dateTime.format(formatTime);

		try {
			// save new file to saves folder
			String filePath = "saves\\" + formatDateTime + ".txt";
			FileWriter myWriter = new FileWriter(filePath);
			// save the turn number to the first line
			myWriter.write(turnNumber + "\n");
			// for every player save the player attributes separated by ,
			int i = 0;
			for (Player p : pStore.getPlayerList()) {
				myWriter.write(p.getName() + ",");
				myWriter.write(p.getPosition() + ",");
				myWriter.write(p.getBalance() + ",");
				myWriter.write(p.getFreedom() + ",");
				myWriter.write(p.getKey() + ",");
				myWriter.write(p.getPoacherTrapEntryNum() + ",");
				myWriter.write(p.getTrees() + ",");

				// create string with cards owned by player
				// for every card the player owns save its position to denote what card it is,
				// and the number of trees on it
				// each card is separated with |
				// e.g. positionOfCard.NumberOfTrees|NextPosition.0 etc.

				if (p.getCards().size() > 0) {
					String pCardsStr = "";
					for (Card playerCards : p.getCards()) {
						pCardsStr += playerCards.getPos();
						if (playerCards instanceof ALocation) {
							pCardsStr += "belongsTo" + ((ALocation) playerCards).getNoOfTrees() + "Seperation";
						} else {
							pCardsStr += "belongsTo" + 0 + "Seperation";
						}
					}
					if (i == pStore.getPlayerList().size() - 1) {
						myWriter.write(pCardsStr);
					} else {
						myWriter.write(pCardsStr + "\n");
					}
					i++;
				}

				else {
					if (i == pStore.getPlayerList().size() - 1) {
						myWriter.write("-1");
					} else {
						myWriter.write("-1" + "\n");
					}
					
					i++;
					
				}
			}

			myWriter.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
}
