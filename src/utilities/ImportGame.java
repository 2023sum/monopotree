package utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

import card.Board;
import card.Card;
import card.LuckyDipPositionCard;
import card.TreasureChestPositionCard;
import locations.ALocation;
import locations.Transport;
import locations.Utility;
import miscCards.NonLocation;
import miscCards.Tariff;
import backFromTheBrink.GameControl;
import utilities.Menu_2;
import utilities.PlayerStorage;
import person.Player;

public class ImportGame {

	/**
	 * display all files in save folder and ask user to select one using menu class
	 */
	public static void selectFile() {
		try {

			Scanner input = new Scanner(System.in);
			File folder = new File("saves");
			File[] listOfFiles = folder.listFiles();
			String[] dirList = new String[listOfFiles.length];

			if (listOfFiles.length == 0) {
				//validation for console
				System.out.println("\nThere are no Files found \n");
			} else {
				int i = 0;
				for (File fName : listOfFiles) {

					dirList[i] = fName.getName();
					i++;
				}

				Menu_2 fileMenu = new Menu_2("\nPlease Select the file to load", dirList);
				fileMenu.print();

				boolean valid = false;
				do {
					int choice = fileMenu.getOptionChoice(input);
					if (choice > 0 && choice <= listOfFiles.length + 1) {
						valid = true;
						if (!importFile(listOfFiles[choice - 1])) {
							System.out.println("Sorry Import unsuccessful.");
							input.close();
							System.exit(0);
						}
					} else {
						System.out.println("Please enter a valid input: \n");
					}
				} while (!valid);

			}
		} catch (java.lang.NullPointerException e) {
			System.out.println("\nNo Files Found\n");
		}

	}

	/**
	 * 
	 * @param saveFile file object chosen from above by user
	 * @return true if import is succesful
	 */
	private static boolean importFile(File saveFile) {

		int rollNum = 0;
		PlayerStorage pStore = new PlayerStorage();

		try {
			Scanner fileReader = new Scanner(saveFile);

			// retrieve current turn number from first line
			rollNum = Integer.parseInt(fileReader.nextLine());

			// for every line (each player) split the player attributes and reconstruct them
			while (fileReader.hasNextLine()) {
				String[] data = new String[8];
				data = (fileReader.nextLine().split(","));
				

				// create arrayList with players cards
				ArrayList<Card> cards = new ArrayList<Card>();

				Player newPlayer = new Player(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]),
						Boolean.parseBoolean(data[3]), Boolean.parseBoolean(data[4]), Integer.parseInt(data[5]),
						Integer.parseInt(data[6]), cards);

				// take players cards and separate them into individual cards
				if (!data[7].equals("-1")) {
					String[] cardsFromFile = data[7].split("Seperation");
					for (String posAndTree : cardsFromFile) {
						String[] individualCard = posAndTree.split("belongsTo");

						int cardPos = Integer.parseInt(individualCard[0]);
						int cardTreeCount = Integer.parseInt(individualCard[1]);

						Board.changeCardForImport(cardPos, cardTreeCount, newPlayer);
						newPlayer.buyLocation(cardPos);

					}
				}

				pStore.addImportPlayer(newPlayer);
			}
			fileReader.close();
		}

		catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}

		startGameFromImport(rollNum, pStore);
		return true;
	}

	/**
	 * 
	 * @param rollNum the turn to start the new game from
	 * @param pStore  the players in the new game
	 */
	private static void startGameFromImport(int rollNum, PlayerStorage pStore) {
		Scanner input = new Scanner(System.in);
		GameControl.startGame(rollNum, pStore, input);
	}

}
