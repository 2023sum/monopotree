package backFromTheBrink;

import java.util.Scanner;

import person.Player;
import utilities.PlayerStorage;

public class WinnerControl {
	
	
	/**
	 * if a player quits at the end of their turn this method is called it picks the
	 * player with the most seeds as the winner
	 * @param playerStorage - The playerStorage object
	 */
	
	public static void winByQuit(PlayerStorage playerStorage) {
		int winSeeds = -1;
		String winPName = "";
		for (Player p : playerStorage.getPlayerList()) {
			if (p.getBalance() > winSeeds) {
				winSeeds = p.getBalance();
				winPName = p.getName();
			}
		}
		System.out.println("The winner is " + winPName);
		System.out.println("With " + winSeeds + " seeds, Congratulations!");

		System.exit(0);
	}
	
	/**
	 * Displays respective win message depending on how player wins the game
	 * @param player - The player
	 * @param type - The type of win
	 * @param playerStorage - The playerStorage object
	 */

	public static void winMessage(Player player, String type, PlayerStorage playerStorage) {
		
		//out puts to console the players details as well as how they have won
		System.out.println("\nCongratulations " + player.getName() + " you are the winner via the means of " + type
				+ "\nHere are your stats: \n" + playerStorage.showGameStats(playerStorage.returnPlayerPosition(player)) + "\n");

	}

	
	/**
	 * checks on each turn to see if the game has reached a winner
	 * @param playerStorage - The object storing the players
	 * @param input - The scanner for input
	 * @param player - The player object
	 */
	public static void winSituation(PlayerStorage playerStorage, Scanner input, Player player) {
		for (Player name : playerStorage.getPlayerList()) {

			//checks if player has won via having a large balance
			if (name.getBalance() >= 4000) {
				String type = "First to 4000 bank Balance";
				winMessage(name, type, playerStorage);
				System.out.println("\nGoodbye!");
				System.exit(0);
			}
			//checks if player is simply the last player in the game and winner by default
			if (playerStorage.getPlayerList().size() < 2) {
				String type = "Last man standing";
				winMessage(name, type, playerStorage);
				System.out.println("\nGoodbye!");
				System.exit(0);
			}

		}
	}

}
