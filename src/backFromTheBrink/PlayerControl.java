package backFromTheBrink;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import card.Board;
import card.Card;
import locations.ALocation;
import locations.Transport;
import locations.Utility;
import person.Player;
import utilities.PlayerStorage;

public class PlayerControl {

	/**
	 * Adding a player into the game
	 * 
	 * @param playerStorage - the playerStorage
	 */

	public static void addPlayer(PlayerStorage playerStorage, Scanner input) {
		if (playerStorage.getAmountOfPlayers() >= 8) {
			System.out.println("\nYou have reached the maximum amount of players\n");
			return;
		}
		boolean nameCheck = false;
		System.out.println("\nPlease enter player name: ");
		String name = input.nextLine();

		// Search through the list of existing players
		for (int i = 0; i < playerStorage.getPlayerList().size(); i++) {
			String usedName = playerStorage.getPlayerList().get(i).getName();
			// Checks if the name the user has entered matches this pre-existing name
			if (name.equals(usedName)) {
				System.out.println("\nThis name has already been used. Please try again.");
				nameCheck = true;
				addPlayer(playerStorage, input);
			}
		}

		if (!name.matches("^[a-zA-Z]*$")) {
			System.out.println("\nInvalid name, only characters a-z allowed");
			nameCheck = true;
			addPlayer(playerStorage, input);
		}

		if (nameCheck == false) {
			playerStorage.addPlayer(name); // add player to the game
		}
		return;
	}

	/**
	 * Removes player from the game where needed
	 * 
	 * @param player
	 * @param playerStorage once a player has left the game a winder is declared
	 */

	public static void removePlayer(Player player, PlayerStorage playerStorage) {

		// updates players array list by removing player
		playerStorage.deletePlayer(player);

		// declares winner
		WinnerControl.winByQuit(playerStorage);
	}

	/**
	 * Checks if player has ran out of seeds(seedless)
	 * 
	 * @param playerStorage - the player storage object
	 * @param player        - a player object
	 * @param Amount        - amount that will be taken
	 */
	public static int tryTakeSeeds(PlayerStorage playerStorage, Player player, int Amount) {
		if (player.takeSeeds(Amount)) {
			return 0;
		} else {
			// removes player if they have run out of seeds (i.e. they have lost the game)
			PlayerControl.removePlayer(player, playerStorage);
			System.out.println(player.getName() + " has run out of seeds!");
			return -1;
		}
	}

	/**
	 * Checks if player has run out of seeds(seedless)
	 * 
	 * @param playerStorage   - the player storage object
	 * @param player          - player giving seeds
	 * @param Amount          - amount to give
	 * @param recievingPlayer - the receiving player
	 */
	public static int tryGiveSeeds(PlayerStorage playerStorage, Player player, int Amount, Player recievingPlayer) {
		if (player.giveSeedsToPlayer(Amount, recievingPlayer)) {
			return 0;
		} else {
			// removes player if they have run out of seeds(i.e they have lost the game)
			PlayerControl.removePlayer(player, playerStorage);
			System.out.println(player.getName() + " has run out of seeds!");
			return -1;
		}
	}

	public static boolean allColourOwned(Card card, Player player, PlayerStorage playerStorage, Scanner input) {

		ALocation location = (ALocation) card;
		boolean allColourOwned = true;
		List<Card> allLocations = Board.getAllLocations();

		// check for every card on the board
		for (int i = 0; i < allLocations.size(); i++) {
			ALocation checkLoc = (ALocation) allLocations.get(i);

			// if colour of another card matches colour of landed on location
			if (checkLoc.getColour().equals(location.getColour())) {

				if (java.util.Objects.equals(checkLoc.getOwnedBy(), player) == false) {
					allColourOwned = false;
					break;
				} else {
					// not owned by current player
					if (checkLoc.getOwnedBy().equals(player)) {
						allColourOwned = true;
					} else {
						allColourOwned = false;
						break;
					}
				}
			}
		}
		return allColourOwned;
	}

	/**
	 * Allows the player to purchase the location they land on
	 * 
	 * @param playerStorage - the player storage object
	 * @param input         - scanner for input
	 * @param player        - player object
	 * @param card          - card that will be bought
	 */

	public static void buyLocation(PlayerStorage playerStorage, Scanner input, Player player, Card card) {
		int price;
		// decides whether location is purchasable(plantable) and what type of
		// purchasable location it is (i.e utility or location)
		try {
			ALocation location = (ALocation) card;
			price = location.getPrice();
			System.out.println("\n" + location.getFact());
		} catch (java.lang.ClassCastException e) {
			try {
				Transport transport = (Transport) card;
				price = transport.getPrice();

			} catch (java.lang.ClassCastException f) {
				try {
					Utility utility = (Utility) card;

					price = utility.getCost();
				} catch (java.lang.ClassCastException n) {
					return;
				}
			}
		}

		if (card.getObtainable() == false) {
			System.out.println("\nPrice Is " + price);
			System.out.println("\n\n1. Buy Location");
			System.out.println("\nANY OTHER KEY. Just Stay Here");

			String choice = input.nextLine();
			if (choice.equals("1")) {
				if (player.getBalance() > price) {
					// adds location to players location list and removes the cost of that location
					// from players seeds
					player.buyLocation(player.getPosition());

					Card selectedCard = Board.getACard(player.getPosition());
					// Setting who owns this card
					if (selectedCard instanceof ALocation) {
						ALocation location = (ALocation) selectedCard;
						location.setOwnedBy(player);
					}
					if (selectedCard instanceof Utility) {
						Utility utility = (Utility) selectedCard;
						utility.setOwnedBy(player);
					}
					if (selectedCard instanceof Transport) {
						Transport station = (Transport) selectedCard;
						station.setOwnedBy(player);
					}

					player.takeSeeds(price);
					System.out.println(player.getName() + " Balance is now: " + player.getBalance());
					System.out.println("~~~~~~~~~~~");
					System.out.println("\n");

					// card.setCanBuy(false);
					card.setObtainable(true);
				}

			} else {
				System.out.print("\nPlayer Stays here\n");
				System.out.println("\n~~~~~~~~~~~");
				System.out.println("\n");
			}
		} else {
			try {
				ALocation location = (ALocation) card;
				// takes donation from a player if they landed on another players location
				System.out.println("\n" + location.takeDonation(player) + " seeds have been taken as a donation");
			} catch (java.lang.ClassCastException e) {
				try {
					Transport transport = (Transport) card;
					System.out.println("\n" + transport.takeDonation(player) + " seeds have been taken as a donation");
				} catch (java.lang.ClassCastException f) {
					Utility utility = (Utility) card;

					System.out.println("\n" + utility.takeDonation(player.rollNoPosChange(), player)
							+ " seeds have been taken as a donation");

					if (utility.takeDonation(player.rollNoPosChange(), player) == 0) {
						PlayerControl.removePlayer(player, playerStorage);
					}
				}
			}
			// Display new balance
			System.out.println(player.getName() + " Balance is now: " + player.getBalance());
		}
	}

	/**
	 * allows the player to purchase a location from another player
	 * 
	 * @param playerStorage
	 * @param input
	 * @param player
	 */
	public static void buyPlayersCard(PlayerStorage playerStorage, Scanner input, Player player) {

		ArrayList<Player> playersWithLocations = new ArrayList<Player>();
		// adds players location to another players cardList
		boolean bool = true;
		while (bool) {
			System.out.println("\nSelect a Player to haggle with: ");
			int count = 1;
			// displays all players except the one who requested the list
			for (Player name : playerStorage.getPlayerList()) {
				if (name.getName() != player.getName()) {
					if (name.getCards().size() >= 1) {
						System.out.println(count + ". " + name.getName());
						playersWithLocations.add(name);
						count++;
					}
				}
			}

			if (count == 1) {
				System.out.println("No one has any locations");
				return;
			}
			int choice = 0;
			System.out.println("\nChoice: ");

			try {
				choice = Integer.parseInt(input.nextLine());

				// stops player being able to buy a location from themselves if above check
				// fails
				if (choice > playersWithLocations.size() || choice < 1) {
					System.out.println("\nInput must be on the list!");
				}
				for (int i = 0; i < playersWithLocations.size(); i++) {
					if (playersWithLocations.get(i).getName().equals(player.getName())) {
						System.out.println("\nYou can't buy your own Location! ");
						return;
					}

					else {

						Player selectedPlayer = playersWithLocations.get(choice - 1);
						System.out.println("\nYou have selected: " + selectedPlayer.getName());
						count = 1;
						// displays selected players cards
						for (Card cards : selectedPlayer.getCards()) {
							System.out.println(count + ". " + cards.getName());
						}
						System.out.println("\nChoice: ");
						int choice3 = 0;
						try {
							choice3 = Integer.parseInt(input.nextLine());
						} catch (InputMismatchException nfe) {
							System.out.println("Please input an integer.");
						}

						if (choice3 > selectedPlayer.getCards().size() || choice3 < 1) {
							System.out.println("\nInput must be on the list!");
						} else {
							Card selectedCard = selectedPlayer.getCards().get(choice3 - 1);
							System.out.println("\nYou have selected: " + selectedCard.getName());
							System.out.println("\nFor how much will this location be sold for?");
							int priceChoice = 0;
							try {
								priceChoice = Integer.parseInt(input.nextLine());
							} catch (InputMismatchException nfe) {
								System.out.println("Please input an integer");
							}

							System.out.println("\nAre you sure ?\n" + "1. Yes\n" + "2. No\n");

							int choice5 = 0;
							try {
								choice5 = Integer.parseInt(input.nextLine());
							} catch (InputMismatchException nfe) {
								System.out.println("Please input an integer");
							}

							if (choice5 == 1) {
								// removes location from selected player and adds it to the new player aswell as
								// removing seeds respectfully
								player.buyLocationPlayer(selectedCard, priceChoice);

								// Setting who owns this card
								if (selectedCard instanceof ALocation) {
									ALocation location = (ALocation) selectedCard;
									location.setOwnedBy(player);
								}
								if (selectedCard instanceof Utility) {
									Utility utility = (Utility) selectedCard;
									utility.setOwnedBy(player);
								}
								if (selectedCard instanceof Transport) {
									Transport station = (Transport) selectedCard;
									station.setOwnedBy(player);
								}

								selectedPlayer.sellLocation(selectedCard, priceChoice);
								System.out.print("\nTransaction Completed!");
								bool = false;
								return;
							}
							if (choice5 == 0) {
								System.out.println("\nTransaction terminated!");

							} else if (choice > 1 || choice < 0) {
								System.out.println("Please choose either 1 or 0\n");
							}
						}
					}
				}
			}
			// checks if input is a number
			catch (java.lang.NumberFormatException e) {
				System.out.println("\nPlease enter a number");
			}
		}
	}

	/**
	 * Lets the player buy houses on their location
	 * 
	 * @param player        - Player
	 * @param card          - current location
	 * @param playerStorage - playerStorage
	 */

	public static void buyTrees(Player player, PlayerStorage playerStorage, Scanner input) {

		// Getting the players owned locations
		ArrayList<ALocation> playerLocations = new ArrayList<ALocation>();
		for (int i = 0; i < player.getCards().size(); i++) {
			try {
				ALocation location = (ALocation) player.getCards().get(i);
				playerLocations.add(location);
			} catch (java.lang.ClassCastException nfe) {

			}
		}

		// Checking if the player has enough locations
		if (playerLocations.size() == 0) {
			System.out.println("You don't have any locations to put trees onto");
			return;
		}

		boolean flag = true;
		while (flag) {
			try {

			// Asks which location to place a tree on
			System.out.println("Which location would you like to add a tree in?\n");
			int treePrice;
			int quitNumber = playerLocations.size() + 1;
			for (int i = 0; i < playerLocations.size(); i++) {
				System.out.println(i + 1 + ". " + playerLocations.get(i).getName() + ": Price of tree is "
						+ playerLocations.get(i).getTreePrice());
			}
			System.out.println(quitNumber + ". " + "Quit this menu");
			int locationChoice = Integer.parseInt(input.nextLine());

			if ((locationChoice) == quitNumber) {
				System.out.println("You have chosen to quit, and not buy any trees");
				return;
			}
			ALocation location = playerLocations.get(locationChoice - 1);
			treePrice = location.getTreePrice();

			// player owns all cards of a particular colour
			if (allColourOwned(location, player, playerStorage, input) == true) {
				// Checks if the player has enough seeds to buy a tree
				if (player.getBalance() < treePrice) {
					System.out.println("You don't have enough seeds to buy a tree");
					return;
				}

				// If the player has less than four trees then they can buy trees
				if (player.getTrees() < 4) {
					// Asks if the user would like to buy a tree
					System.out.println("Would you like to buy a tree? \n1. Yes\n2. No");
					int choice = Integer.parseInt(input.nextLine());
					// Buys the tree
					if (choice == 1) {
						if (player.buyTree() == true) {
							location.addTree();
							player.takeSeeds(treePrice);
							System.out.println("You have bought a tree \n\n");
						}

						else {
							System.out.println("You can't place a tree here");
							flag = false;
						}
					} else if (choice == 2) {
						flag = false;
					}
				}
				// If the player has four trees then they can buy a forest
				else if (player.getTrees() == 4) {
					// Asks if the user would like to buy a forest
					System.out.println("Would you like to buy a forest? \n1. Yes\n2. No");
					int choiceForest = Integer.parseInt(input.nextLine());
					// Buys the forest
					if (choiceForest == 1) {
						if (player.buyTree() == true) {
							location.addTree();
							player.takeSeeds(treePrice);
							System.out.println("You have bought a forest \n\n");
							// Doesn't have to ask if they want to buy another forest because they can't buy
							// another
							flag = false;
						}

						else {
							System.out.println("You can't place a forest here");
							flag = false;
						}
					}
				}

				else {
					flag = false;
				}
			} else
				System.out.println("\nYou do not own all the cards in this set. You cannot yet add any trees\n");

		}
		
		catch(java.lang.NumberFormatException |  java.lang.IndexOutOfBoundsException e)
		{
			System.out.println("Please enter a number on the list!");
		}
	}
}
}
