package backFromTheBrink;

import java.util.Scanner;

import card.Board;
import card.Card;
import locations.ALocation;
import locations.Transport;
import locations.Utility;
import miscCards.GoToPoacherTrap;
import person.Player;
import utilities.PlayerStorage;

public class CardControl {
	
	

	/**
	 * Opens Treasure chest depending on players roll
	 * @param index - Random integer to choose which treasure chest is used
	 * @param playerStorage - The storage class for players
	 * @param player - The player that landed on this card
	 * @param rollNumber - What the player rolled
	 * @param playerIndex - Which player this is in the playerStorage playerList
	 */
	public static void useTreasureChest(int index, PlayerStorage playerStorage, Player player, int rollNumber, int playerIndex) {
		
		// switch statement reads certain chest depending on what players roll is and does what chest says respectfully
		switch (index) {
		case 0:
			player.addSeeds(200);
			System.out.println("200 seeds were added to " + player.getName() + "'s balance!");
			player.setPosition(0);
			break;
		case 1:
			System.out.println(GoToPoacherTrap.goPoacherTrap(player, rollNumber));
			break;
		case 2:
			player.addSeeds(100);
			System.out.println("100 seeds were added to " + player.getName() + "'s balance!");
			break;
		case 3:
			System.out.println("50 seeds were taken from " + player.getName() + "'s balance!");
			PlayerControl.tryTakeSeeds(playerStorage, player, 50);
			break;
		case 4:
			player.addSeeds(100);
			System.out.println("100 seeds were added to " + player.getName() + "'s balance!");
			break;
		case 5:
			player.addSeeds(50);
			System.out.println("50 seeds were added to " + player.getName() + "'s balance!");
			break;
		case 6:
			for (int i = 0; i < playerStorage.getPlayerList().size(); i++) {
				if (i != playerIndex) {
					playerStorage.getPlayerList().get(i).giveSeedsToPlayer(10, player);
				}
			}
			break;
		case 7:
			player.addSeeds(20);
			System.out.println("20 seeds were added to " + player.getName() + "'s balance!");
			break;
		case 8:
			player.addSeeds(100);
			System.out.println("100 seeds were added to " + player.getName() + "'s balance!");
			break;
		case 9:
			System.out.println("80 seeds were taken from " + player.getName() + "'s balance!");
			PlayerControl.tryTakeSeeds(playerStorage, player, 80);
			break;
		case 10:
			player.addSeeds(20);
			System.out.println("20 seeds were added to " + player.getName() + "'s balance!");
			break;
		case 11:
			System.out.println("40 seeds were taken from " + player.getName() + "'s balance!");
			PlayerControl.tryTakeSeeds(playerStorage, player, 40);
			break;
		case 12:
			player.addSeeds(50);
			System.out.println("50 seeds were added to " + player.getName() + "'s balance!");
			break;
		case 13:
			player.addSeeds(25);
			System.out.println("25 seeds were added to " + player.getName() + "'s balance!");
			break;
		case 14:
			System.out.println("100 seeds were taken from " + player.getName() + "'s balance!");
			PlayerControl.tryTakeSeeds(playerStorage, player, 100);
			break;
		case 15:
			int noOfTrees = 0;
			int noOfForests = 0;
			for(int i = 0; i < player.getCards().size(); i++)
			{
				try {
					ALocation location = (ALocation) player.getCards().get(i);
					if(location.getNoOfTrees() < 5)
					{
						noOfTrees += location.getNoOfTrees();
					}
					else
					{
						noOfForests += 1;
					}
					
				}
				catch(java.lang.ClassCastException nfe)
				{
					
				}
			player.takeSeeds(noOfTrees * 10);
			player.takeSeeds(noOfForests * 20); 
			}
			break;
		default:
			System.out.println("Not an option ERROR");

		}
	}

	
	
	/**
	 * Opens luckdip depending on what the player has rolled
	 * @param index
	 * @param playerStorage
	 * @param player
	 * @param input
	 * @param rollNumber
	 */
	public static void useLuckyDip(int index, PlayerStorage playerStorage, Player player, Scanner input, int rollNumber, int playerIndex) {
		
		// switch statement reads certain luckydip depending on what players roll is and does what luckyDip says respectfully
		switch (index) {
		case 0:
			player.addSeeds(200);
			System.out.println("200 seeds were added to " + player.getName() + "'s balance!");
			player.setPosition(0);
			break;
		case 1:
			if (player.getPosition() <= 29) {
				player.setPosition(29);
				System.out.println("Moved to GANSU!");
			} else {
				player.addSeeds(200);
				System.out.println("200 seeds were added to " + player.getName() + "'s balance!");
				player.setPosition(29);
				System.out.println("Moved to GANSU!");
			}
			break;
		case 2:
			if (player.getPosition() <= 24) {
				player.setPosition(24);
				System.out.println("Moved to PARAGUAY!");
			} else {
				player.addSeeds(200);
				System.out.println("200 seeds were added to " + player.getName() + "'s balance!");
				player.setPosition(24);
				System.out.println("Moved to PARAGUAY!");
			}
			break;
		case 3:		
			if (player.getPosition() <= 11) {
				player.setPosition(11);
				System.out.println("Moved to CHAD!");
			} else {
				player.addSeeds(200);
				System.out.println("200 seeds were added to " + player.getName() + "'s balance!");
				player.setPosition(11);
				System.out.println("Moved to CHAD!");
			}
			break;
		case 4:
			if (player.getPosition() <= 12) {
				player.setPosition(12);
				System.out.println("Moved to Solar Power!");
			} else if (player.getPosition() > 12 && player.getPosition() <= 28) {
				player.setPosition(28);
				System.out.println("Moved to Hydro Power!");
			} else if (player.getPosition() > 28) {
				player.addSeeds(200);
				System.out.println("200 seeds were added to " + player.getName() + "'s balance!");
				player.setPosition(12);
				System.out.println("Moved to Solar Power!");

			}

			break;
		case 5:
			if (player.getPosition() <= 5) {
				player.setPosition(5);
				System.out.println("Moved to Cycle Path!");
			} else if (player.getPosition() > 5 && player.getPosition() <= 15) {
				player.setPosition(15);
				System.out.println("Moved to Boat Club!");
			} else if (player.getPosition() > 15 && player.getPosition() <= 25) {
				player.setPosition(25);
				System.out.println("Moved to Electric Car!");

			} else if (player.getPosition() > 25 && player.getPosition() <= 35) {
				player.setPosition(35);
				System.out.println("Moved to Paraglider!");
			} else if (player.getPosition() > 35) {
				player.addSeeds(200);
				System.out.println("200 seeds were added to " + player.getName() + "'s balance!");
				player.setPosition(5);
				System.out.println("Moved to Cycle Path!");
			}

			break;
		case 6:
			player.addSeeds(50);
			System.out.println("50 seeds were added to " + player.getName() + "'s balance!");
		case 7:
			player.setKey(true);
			break;
		case 8:
			if(player.getPosition() >= 3)
			{
				player.setPosition(player.getPosition() - 3);
			}
			else {
				int positionHolder = player.getPosition() - 3;
				// Positive because you're adding on a negative number
				player.setPosition(39 + positionHolder);
			}
			System.out.println("Player moved back 3 spaces!");
			break;
		case 9:
			player.setPosition(39);
			System.out.println("Moved to QUEENSLAND!");
			break;
		case 10:
			System.out.println(GoToPoacherTrap.goPoacherTrap(player, rollNumber));
			break;
		case 11:
			int noOfTrees = 0;
			int noOfForests = 0;
			for(int i = 0; i < player.getCards().size(); i++)
			{
				try {
					ALocation location = (ALocation) player.getCards().get(i);
					if(location.getNoOfTrees() < 5)
					{
						noOfTrees += location.getNoOfTrees();
					}
					else
					{
						noOfForests += 1;
					}
					
				}
				catch(java.lang.ClassCastException nfe)
				{
					
				}
			player.takeSeeds(noOfTrees * 25);
			player.takeSeeds(noOfForests * 25); 
			}
			break;
		case 12:
			System.out.println("50 seeds were taken from " + player.getName() + "'s balance!");
			PlayerControl.tryTakeSeeds(playerStorage, player, 50);
			break;
		case 13:
			if (player.getPosition() <= 5) {
				player.setPosition(5);
				System.out.println("Moved to CYCLE PATH!");
			} else {
				player.addSeeds(200);
				System.out.println("200 seeds were added to " + player.getName() + "'s balance!");
				player.setPosition(5);
				System.out.println("Moved to CYCLE PATH!");
			}
			break;
		case 14:
			System.out.println("50 seeds given to all players!");
			for (int i = 0; i < playerStorage.getPlayerList().size(); i++) {
				if (i != playerIndex) {
					PlayerControl.tryGiveSeeds(playerStorage, player, 50, playerStorage.getPlayerList().get(i));
				}
			}
			break;
		case 15:
			player.addSeeds(20);
			System.out.println("20 seeds were added to " + player.getName() + "'s balance!");
			break;
		default:
			System.out.println("Not an option ERROR");
		}
	}

}
