package backFromTheBrink;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

import card.Board;
import card.Card;
import card.LuckyDipPositionCard;
import card.TreasureChestPositionCard;
import locations.ALocation;
import miscCards.GoToPoacherTrap;
import miscCards.Tariff;
import person.Player;
import utilities.ExportGame;
import utilities.PlayerStorage;

public class GameControl {

	/**
	 * Starting the game
	 * 
	 * @param rollNumber    - The amount of rolls the game has gone through
	 * @param playerStorage - The object storing the players
	 * @param input         - The scanner for input
	 */

	public static void startGame(int rollNumber, PlayerStorage playerStorage, Scanner input) {

		while (true) {
			// Incrementing amount of turns to calculate who's turn it is
			rollNumber++;
			// Calculating who's turn it is
			Game.playerIndex = rollNumber % playerStorage.getPlayerList().size();

			Player player = playerStorage.getPlayerList().get(Game.playerIndex);
			WinnerControl.winSituation(playerStorage, input, player);
			boolean rollingInPoacherTrap = false;
			// Playing the players turn

			GameControl.turn(player, playerStorage, input, rollNumber, rollingInPoacherTrap);
		}

	}

	public static void subMenu(Player player, PlayerStorage playerStorage, Scanner input, int rollNumber,
			boolean rollingInPoacherTrap) {

		System.out.println("\nWhat do you want to do next ?");
		System.out.println("\n1. See Cards");
		System.out.println("\n2. Buy Players Cards");
		System.out.println("\n3. Buy trees on any properties");
		System.out.println("\n4. Save and Quit");
		System.out.println("\n5. Quit without saving");
		System.out.println("\nANY OTHER KEY. Next turn");

		String choice = input.nextLine();

		if (choice.equals("1")) {

			if (player.getCards().size() == 0) {
				System.out.println("\nYou own no cards to show.");
			} else {
				System.out.println("\nCARDS:\n");
				for (int i = 0; i < player.getCards().size(); i++) {
					try {
						ALocation location = (ALocation) player.getCards().get(i);
						System.out.println(i + 1 + ". " + player.getCards().get(i).getName() + " ("
								+ location.getColour() + " card)");
					} catch (java.lang.ClassCastException e) {
						System.out.println(i + 1 + ". " + player.getCards().get(i).getName());
					}
				}
			}
			System.out.println("\n");
			subMenu(player, playerStorage, input, rollNumber, rollingInPoacherTrap);

		} else if (choice.equals("2")) {
			PlayerControl.buyPlayersCard(playerStorage, input, player);
			subMenu(player, playerStorage, input, rollNumber, rollingInPoacherTrap);
		} else if (choice.equals("3")) {
			PlayerControl.buyTrees(player, playerStorage, input);
			subMenu(player, playerStorage, input, rollNumber, rollingInPoacherTrap);
		} else if (choice.equals("4")) {
			ExportGame.SaveGame(playerStorage, rollNumber);
			System.out.println("\nThe game has been saved.");
			System.out.println("Goodbye!");
			System.exit(1);

		} else if (choice.equals("5")) {
			WinnerControl.winByQuit(playerStorage);
		} else {

			// break;
			System.out.println("\nEND OF GO\n");
		}
	}

	/**
	 * Players turn during a game
	 * 
	 * @param player        - the player who's turn it is
	 * @param playerStorage - the playerStorage of the game
	 */

	public static void turn(Player player, PlayerStorage playerStorage, Scanner input, int rollNumber,
			boolean rollingInPoacherTrap) {

		boolean leavePoacherTrapStop = false;
		System.out.println("\n---------------------------------------------------");
		if (player.getFreedom() == true || rollingInPoacherTrap == true) {

			int doubleRollMax = 0;
			boolean doubleRoll = true;

			do {
				doubleRoll = false;
				Random rand = new Random();
				System.out.println(playerStorage.showGameStats(Game.playerIndex)); // shows players name position and
																					// balance
				int firstRoll = player.rollNoPosChange(); // Rolling two dice for the write player
				int secondRoll = player.rollNoPosChange();

				// Outputting to the user to tell them what they rolled
				System.out.println("\n" + player.getName() + " rolled: " + firstRoll + " and " + secondRoll + "\n");

				// double is rolled
				if (firstRoll == secondRoll) {
					doubleRollMax++; // counting doubles rolled in a row
					doubleRoll = true;
					// rollingInPoacherTrap = false;

					System.out.println(doubleRollMax + " double(s) - YOU HAVE ROLLED A DOUBLE. ");

					if (rollingInPoacherTrap == false)
						System.out.println("THIS WILL ALLOW YOU TO TAKE ANOTHER TURN");

					else {
						System.out.println("\n\nYOU ARE NOW OUT OF POACHER TRAP\n\n");
						player.setFreedom(true); // released from poacher trap
						rollingInPoacherTrap = false;
						// allows to move forward number of spaces rolled and the players turn ends
						leavePoacherTrapStop = true;
					}
				}
				// not in poacher trap
				if (rollingInPoacherTrap == false) {

					if (doubleRollMax >= 3) {
						System.out.println(
								"\nUh Oh! You have rolled your third double in a row.\nThis means you have to GO TO POACHER TRAP\n\n");
						// GO TO POACHER TRAP
						System.out.println(GoToPoacherTrap.goPoacherTrap(player, rollNumber));
						break;
					}

					int roll = firstRoll + secondRoll; // total roll
					System.out.println("\nMoving " + (roll) + " Spaces ...");
					int original = player.getPosition();
					player.move(firstRoll);
					player.move(secondRoll);
					int newPos = player.getPosition();
					Card card = Board.getCards().get(player.getPosition());

					System.out.println("\nLanded on " + card.getName());
					// Checking if the card is a location
					boolean isLocation = true;
					try {
						ALocation location = (ALocation) card;
						System.out.println("(" + location.getColour() + " card)");
					} catch (java.lang.ClassCastException e) {
						isLocation = false;
					}

					if (newPos < original) {
						if (card.getName().equals("Garden Centre")) {
							player.addSeeds(200);
							System.out.println("200 seeds was added to " + player.getName() + "'s balance!");
						} else {
							System.out.println("Passed Garden Centre!");
							player.addSeeds(200);
							System.out.println("200 seeds was added to " + player.getName() + "'s balance!");
						}
					}

					if (card.canBuy() == true) {
						PlayerControl.buyLocation(playerStorage, input, player, card);

					} else if (card.canBuy() == false) {

						if (card.getName().equals("Tariff")) {
							System.out.println(
									"A tariff of 200 seeds was taken from " + player.getName() + "'s balance!");
							PlayerControl.tryTakeSeeds(playerStorage, player, Tariff.getTariffPayment());
						}
						if (card.getName().equals("luckyDip")) {
							int index = rand.nextInt(16);
							System.out.println(LuckyDipPositionCard.getCard(index));
							// move to use card in LuckyDipPositionCard
							CardControl.useLuckyDip(index, playerStorage, player, input, rollNumber, Game.playerIndex);
						}
						if (card.getName().equals("TreasureChest")) {
							int index = rand.nextInt(16);
							System.out.println(TreasureChestPositionCard.getCard(index));
							CardControl.useTreasureChest(index, playerStorage, player, rollNumber, Game.playerIndex);
						}

						if (card.getName().equals("Go to Poacher trap")) {
							System.out.println(GoToPoacherTrap.goPoacherTrap(player, rollNumber));
						}
						if (card.getName().equals("Poacher trap")) {
							if (player.getFreedom() == true) {
								System.out.println("Just visiting!");
							} else {
								System.out.println("\nYOU ARE IN A POACHERS TRAP !!!");
							}
						}
					}
					subMenu(player, playerStorage, input, rollNumber, rollingInPoacherTrap);
				}
			} while (doubleRollMax < 3 && doubleRoll == true && rollingInPoacherTrap == false
					&& leavePoacherTrapStop == false);
		}

		// IN POACHER TRAP
		else {
			// number of turns needed to have passed = number of players multiplied by 3
			int turnsPassedNeeded = playerStorage.getPlayerList().size() * 3;
			int exitRollCount = turnsPassedNeeded + player.getPoacherTrapEntryNum();
			int turnsLeft = (exitRollCount - rollNumber) / playerStorage.getPlayerList().size();
			rollingInPoacherTrap = true;

			System.out.println("\n\n" + player.getName()
					+ "\nFor your chance at getting out of poacher trap, you have to roll a double\nIf you are successful"
					+ " you will move forawrd the amount of spaces you roll.\nYour turn will be over and you will not roll again");

			// allow the player to roll the dice
			turn(player, playerStorage, input, rollNumber, rollingInPoacherTrap);

			// released from poacher trap
			if (exitRollCount == rollNumber || rollingInPoacherTrap == false) {
				System.out.println(player.getName() + " you are out of poacher trap, you are now \"just visting\"");
				player.setFreedom(true);
				player.setPoacherTrapEntryNum(0);
				rollingInPoacherTrap = false;
			} else {
				System.out.println(
						player.getName() + " YOU ARE IN POACHER TRAP. You are out in " + turnsLeft + " full turn(s)\n");
			}
		}
	}

}
