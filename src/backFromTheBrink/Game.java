package backFromTheBrink;

import java.util.Scanner;
import card.Board;
import utilities.ImportGame;
import utilities.Menu_2;
import utilities.PlayerStorage;

public class Game {

	/**
	 * Static variables that the main methods need to work
	 */

	static int playerIndex = 0; // Amount of players in the game

	/**
	 * Main game method used to control the whole game
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Declaring the scanner
		PlayerStorage playerStorage = new PlayerStorage();
		String title = "\nBack From the Brink"; // Title for the menu
		String options[] = { "Start game", "Add player", "Import Saved Game", "Quit game", }; // Options in the menu
		Menu_2 menu = new Menu_2(title, options); // Declaring the menu
		int rollNumber = 0; // Counting how many dice rolls have went through
		Board.initialiseLocations(); // Initialising the locations on the boat
		boolean first = true;
		
		// Main game loop (where the game starts)
		while (true) {
			// Outputs the menu
			menu.print();
			// Gets the choice from the menu
			int choice = menu.getOptionChoice(input);
			// Game starts
			if (choice == 1) {

				// Validation for if there haven't been enough people added to play the game
				if (playerStorage.getPlayerList().size() <= 1) {
					System.out.println("\nPlease add at least two players\n");
				} else {
					while (true) {
						// Incrementing amount of turns to calculate who's turn it is
						//rollNumber++;
						// Starting the game
						GameControl.startGame(rollNumber, playerStorage, input);
					}
				}
			}
			if (choice == 2) {
				if (first) {
					boolean input2 = true;
					boolean inputValidate = true;

					do {
						PlayerControl.addPlayer(playerStorage, input);
						do {
							if (playerStorage.getAmountOfPlayers() >= 8) {
								System.out.println("\nYou have reached the maximum amount of players\n");
								input2 = false;
								inputValidate = false;
								first = false;
							} else {
								// Asking if the user would like to add another player
								System.out.println("\nWould you like to add another player? \n1 --> Yes\n2 --> No");
								// String inputS = input.nextLine();
								int inputC = menu.getOptionChoice(input);
								// Exiting the loop if the user doesn't want to add another player
								if (inputC == (2)) {
									input2 = false;
									inputValidate = false;
								}
								// if the user does want to add another player
								else if (inputC == (1)) {
									input2 = true;
									inputValidate = false;
								}
								// if the user's choice does not match a given option
								else {
									System.out.println("This is not a valid choice. Please try again.");
									inputValidate = true;
								}
							}
						} while (inputValidate);
					} while (input2);
				} else {
					System.out.println("\nYou have reached the maximum amount of players\n");
				}
			}
			// Game quits
			if (choice == 4) {
				System.out.println("\nGoodbye!");
				input.close();
				System.exit(0);
			}
			// Import a saved Game
			if (choice == 3) {
				ImportGame.selectFile();
			}

		}

	}

}
