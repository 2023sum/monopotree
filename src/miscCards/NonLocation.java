package miscCards;



public class NonLocation extends MiscCards {
	private NonLocations typeOfCard;		// The type of the NonLocation card
	
	/**
	 * Constructor to instantiate a NonLocation
	 * @param name - Name of the card
	 * @param position - Position of the card on the board
	 * @param obtainable - If the card is obtainable or not
	 */
	
	public NonLocation(String name, int position, boolean obtainable, boolean canBuy)
	{
		super(name,position,obtainable,canBuy);
		setType(name);
		this.obtainable = obtainable;
	}
	
	/**
	 * Setting the type of the NonLocation card
	 * @param name - The name of the card from the .csv file
	 */
	
	public void setType(String name)
	{
		if(name.equals("Garden Centre"))
		{
			this.typeOfCard = NonLocations.GardenCentre;
		}
		if(name.equals("Poacher trap"))
		{
			this.typeOfCard = NonLocations.PoacherTrap;
		}
		if(name.equals("Public Park"))
		{
			this.typeOfCard = NonLocations.PublicPark;
		}
		if(name.equals("Go to Poacher trap"))
		{
			this.typeOfCard = NonLocations.GoToPoacherTrap;
		}
	}
	
	public NonLocations getTypeOfCard()
	{
		return this.typeOfCard;
	}
	
	
	/**
	 * Sending the user to the poacher trap on the board 
	 * @return - if the method was successful or not
	 */
	
	public boolean GoPoacherTrap()
	{
		return false;
	}
	

}
