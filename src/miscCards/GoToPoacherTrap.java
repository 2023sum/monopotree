package miscCards;

import person.Player;

public class GoToPoacherTrap extends MiscCards {
	
	public GoToPoacherTrap(String name, int position, boolean obtainable,boolean canBuy) {
		super(name, position, obtainable,canBuy);
		
	}

	/**
	 * @author Kelsey Forsythe
	 * GoToCard used to send Player to poacher trap position on board
	 */
	
	
	/**
	 * Method to send user to poacher trap or use poacher trap key and avoid
	 * @param player - Player that lands on 'Go to Poacher Trap' on board
	 * @return - boolean true if player sent to poacher trap or false if not
	 */
	
	public static String goPoacherTrap(Player player, int rollNumber) {
		if (player.getKey() == true) {
			player.setKey(false);
			return player.getName() + " has a poacher trap key and escapes!";
			
		} else {
			player.setFreedom(false);
			player.setPosition(10);
			player.setPoacherTrapEntryNum(rollNumber);
			return player.getName() + " has been sent to the poacher's trap!";
		}

	}

	
}
