package miscCards;

public class Tariff extends MiscCards{ 

	private static int tariffPayment = 200;
	
	public Tariff(String name, int position, boolean obtainable, boolean canBuy)
	{
		super(name,position,obtainable,canBuy);

	}
	
	public static int getTariffPayment()
	{
		return tariffPayment;
	}
	
}
