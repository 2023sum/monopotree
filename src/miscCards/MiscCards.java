package miscCards;

import card.Card;



public abstract class MiscCards extends Card{
	
	protected int position;		// Position of the card on the board

	/**
	 * Constructor to instantiate a misc. card
	 * @param name - Name of the card 
	 * @param position - Position of the card on the board
	 * @param obtainable - Boolean if it is obtainable or not
	 */
	
	public MiscCards(String name, int position, boolean obtainable, boolean canBuy)
	{
		super(name,obtainable,canBuy);
		this.position = position;
	}

}
