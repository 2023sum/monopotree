package card;
import utilities.ImportTreasureChest;



public class TreasureChestPositionCard extends Card {
	
	
	private static String treasureChestCards[];
	
	private int positions[];		// Positions which this card can be at
	
	/**
	 * Constructor for the treasure chest object
	 * @param name - String with what the treasure chest will say
	 * @param obtainable - If the card is obtainable or not
	 */
	
	public TreasureChestPositionCard(String name, boolean obtainable, boolean canBuy)
	{
		super(name,obtainable,canBuy);
		treasureChestCards = new String[16];
		treasureChestCards = ImportTreasureChest.importCards();
		positions = new int[3];
		this.positions[0] = 2;
		this.positions[1] = 17;
		this.positions[2] = 33;
		
	}
	
	
	
	
	/**
	 * returns card that is selected
	 * @param index
	 * @return
	 */
	public static String getCard(int index)
	{
		try {
			return treasureChestCards[index];
		}
		catch(Exception e)
		{
			String error = "Index is out of bounds for the list of cards! ";
			return error;
		}
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	
	
}
