package card;



public abstract class Card {
	
	protected boolean obtainable;
	protected String name;
	protected boolean canBuy;
	private int pos;
	
	

	/** 
	 * Card constructor
	 * @param name - Name of the card
	 * @param obtainable - If the card is obtainable or not
	 */
	
	public Card(String name, boolean obtainable, boolean canBuy) 
	{
		this.name = name;
		this.obtainable = obtainable;
		this.canBuy = canBuy;
	}
	
	
	
	/**
	 * Returns the name of the card
	 * @return - Name of the card 
	 */
	
	
	public String getName() {
		return name;
	}
	
	
	/**
	 * Returns if the card is obtainable
	 * @return
	 */
	public boolean getObtainable() {
		return obtainable;
	}
	
	
	/**
	 * checks if card is buyable
	 * @return
	 */
	public boolean canBuy() {
		return canBuy;
	}
	
	/**
	 * sets if can can be bought
	 * @param canBuy
	 */
	public void setCanBuy(boolean canBuy)
	{
		this.canBuy = canBuy;
	}
	
	
	/**
	 * Sets if card is obtainable
	 * @param obtainable
	 */
	public void setObtainable(boolean obtainable)
	{
		this.obtainable = obtainable;
	}
	
	public void setPos(int pos) {
		this.pos = pos;
	}
	
	public int getPos() {
		return this.pos;
	}

}
