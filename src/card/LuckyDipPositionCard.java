package card;
import utilities.ImportLuckyDip;



public class LuckyDipPositionCard extends Card{
	
	
	private static String luckyDipCards[];
	
	
	
	
	private int positions[];		// Positions which this card can be at
	
	/**
	 * Constructor for the lucky dip object
	 * @param name - String with what the treasure chest will say
	 * @param obtainable - If the card is obtainable or not
	 */
	
	
	
	public LuckyDipPositionCard(String name, boolean obtainable, boolean canBuy)
	{
		super(name,obtainable,canBuy);
		luckyDipCards = new String[16];
		luckyDipCards = ImportLuckyDip.importCards();
		positions = new int[3];
		this.positions[0] = 7;
		this.positions[1] = 22;
		this.positions[2] = 36;
		
	}

	
	
	/**
	 * returns card that is requested
	 * @param index
	 * @return
	 */
	public static String getCard(int index)
	{
		try {
		return luckyDipCards[index];
		}
		catch(Exception e)
		{
			String error = "Index is out of bounds for the list of cards! ";
			return error;
		}
	}
	
	
	


}
