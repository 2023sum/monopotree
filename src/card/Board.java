package card;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import locations.Locations;
import locations.ALocation;
import locations.Transport;
import locations.Utility;
import miscCards.NonLocation;
import miscCards.Tariff;
import person.Player;

import java.util.ArrayList;
import java.util.List;

public class Board {
	
	
	// Lists for all of the card objects
	private static List<Card> cards = new ArrayList <Card>();
	private static List<Card> allLocations = new ArrayList <Card>();


	
	//String name, String colour, int cost, int treePrice, int donation0, int donation1, int donation2, int donation3, int donation4, int donationForest, boolean owned, int position, Player ownedBy, boolean obtainable, boolean canBuy
	
	/**
	 * Putting all of the data from the .csv file into it's correct class
	 */
	public static void initialiseLocations() {
		
		String path = "src/files/MonopotreeCardValues.csv";

		Card[][] array = new Card[40][12];
		
		try {

			File dataFile = new File(path);

			Scanner myReader = new Scanner(dataFile);

			for(int i = 0; i < array.length; i++) {
				String[] data = myReader.nextLine().split(",");
				
				// Adding a property
				if(data[11].equals("Location")) {
					ALocation newCard = new ALocation(data[0],data[1],parseToInt(data[2]),parseToInt(data[3]),parseToInt(data[4]),parseToInt(data[5]), parseToInt(data[6]), parseToInt(data[7]), parseToInt(data[8]),parseToInt(data[9]), false, i, null, false,true,data[12]);
					newCard.setPos(i);
					allLocations.add(newCard);
					cards.add(newCard);
				}
				// Adding a train station
				else if(data[11].equals("Transport")) {
					Transport newCard = new Transport(data[0],parseToInt(data[2]),parseToInt(data[5]),parseToInt(data[6]),parseToInt(data[7]),parseToInt(data[8]),false,i,null,false,true,data[12]);
					newCard.setPos(i);
					cards.add(newCard);
				}
				// Adding a utility
				else if(data[11].equals("Utility")) {
					Utility newCard = new Utility(data[0],i,false,parseToInt(data[2]),true, null,data[12]);
					newCard.setPos(i);
					cards.add(newCard);
				}
				// Adding a tariff card
				else if(data[11].equals("Tariff")) {
					Card newCard = new Tariff(data[0],i,true,false);
					newCard.setPos(i);
					cards.add(newCard);
				}
				// Adding a non property card
				else if(data[11].equals("Non-Location")) {
					Card newCard = new NonLocation(data[0],i,true,false);
					newCard.setPos(i);
					cards.add(newCard);
				}
				// Adding a lucky dip card
				else if(data[11].equals("Lucky-Dip")) {
					Card newCard = new LuckyDipPositionCard("luckyDip", true,false);
					newCard.setPos(i);
					cards.add(newCard);
				}
				// Adding a treasure chest card
				else if(data[11].equals("Treasure-Chest")) {
					Card newCard = new TreasureChestPositionCard("TreasureChest",true,false);
					newCard.setPos(i);
					cards.add(newCard);
				}
				
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	/**
	 * Parsing the string data from the .csv file to an integer
	 * @param data - The string from the file
	 * @return - The integer needed to instantiate the class
	 */
	public static int parseToInt(String data)
	{
		return Integer.parseInt(data);
	}
	
	
	public static List<Card> getCards() {
		return cards;
	}

	public static Card getACard(int pos) {
		return cards.get(pos);
	}
	
	public static List<Card> getAllLocations() {
		return allLocations;
	}
	
	public static String ToString() {
		
		String results = "";
		for(Card d : cards) {
			results += "," + d.toString();
		}
		return results;
		
	}
	
	/**
	 * 
	 * @param pos position of the card to be changed
	 * @param treeNum number of trees on that card
	 * @param owner the new owner of the card
	 */
	public static void changeCardForImport(int pos, int treeNum, Player owner) {
		Card currentCard = cards.get(pos);
		if(currentCard instanceof ALocation) {
			((ALocation) currentCard).SetNoOfTrees(treeNum);			
		}
		((Locations) currentCard).setOwnedBy(owner);

	}
	
}
